/*
 * Copyright (c) 2015 Actions Semi Co., Ltd.
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#ifndef __ASM_ARCH_RESET_S900_H__
#define __ASM_ARCH_RESET_S900_H__

/* Module Clock ID */
#define RESET_DMAC				0
#define RESET_SRAMI				1
#define RESET_DDR_CTL_PHY			2
#define RESET_NANDC0				3
#define RESET_SD0				4
#define RESET_SD1				5
#define RESET_PCM1				6
#define RESET_DE				7
#define RESET_LCD0				8
#define RESET_SD2				9
#define RESET_DSI				10
#define RESET_CSI0				11
#define RESET_BISP_AXI				12
#define RESET_CSI1				13
#define RESET_GPIO				15
#define RESET_EDP				16
#define RESET_AUDIO				17
#define RESET_PCM0				18
#define RESET_VDE				19
#define RESET_VCE				20
#define RESET_HDE				21
#define RESET_GPU3D_PA				22
#define RESET_IMX				23
#define RESET_SE				24
#define RESET_NANDC1				25
#define RESET_SD3				26
#define RESET_GIC				27
#define RESET_GPU3D_PB				28
#define RESET_DDR_CTL_PHY_AXI			29
#define RESET_CMU_DDR				30
#define RESET_DMM				31
#define RESET_USB2HUB				32
#define RESET_USB2HSIC				33
#define RESET_HDMI				34
#define RESET_HDCP2TX				35
#define RESET_UART6				36
#define RESET_UART0				37
#define RESET_UART1				38
#define RESET_UART2				39
#define RESET_SPI0				40
#define RESET_SPI1				41
#define RESET_SPI2				42
#define RESET_SPI3				43
#define RESET_I2C0				44
#define RESET_I2C1				45
#define RESET_USB3				46
#define RESET_UART3				47
#define RESET_UART4				48
#define RESET_UART5				49
#define RESET_I2C2				50
#define RESET_I2C3				51
#define RESET_ETHERNET				52
#define RESET_CHIPID				53
#define RESET_I2C4				54
#define RESET_I2C5				55
#define RESET_CPU_SCNT				62

#endif	/* __ASM_ARCH_RESET_S900_H__ */
