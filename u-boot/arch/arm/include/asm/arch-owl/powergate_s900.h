/*
 * Copyright (c) 2015 Actions Semi Co., Ltd.
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#ifndef __ASM_ARCH_POWERGATE_S900_H__
#define __ASM_ARCH_POWERGATE_S900_H__

/* Module Powergate ID */
#define POWERGATE_GPU_B			0
#define POWERGATE_VCE			1
#define POWERGATE_SENSOR		2
#define POWERGATE_VDE			3
#define POWERGATE_HDE			4
#define POWERGATE_USB3			5
#define POWERGATE_DDR0			6
#define POWERGATE_DDR1			7
#define POWERGATE_DE			8
#define POWERGATE_NAND			9
#define POWERGATE_USB2H0		10
#define POWERGATE_USB2H1		11
#define POWERGATE_MAX_ID		POWERGATE_USB2H1

#endif	/* __ASM_ARCH_POWERGATE_S900_H__ */
