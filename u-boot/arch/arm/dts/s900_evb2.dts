/*
 * Copyright (c) 2015 Actions Semi Co., Ltd.
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

/dts-v1/;

#include "s900.dtsi"

/ {
	model = "s900 evb 216ball";
	compatible = "actions,s900_evb_216ball", "actions,s900";

	chosen {
		stdout-path = &serial5;
	};

	clock:e0160000 {
		compatible = "actions,owl-clk";
		core_pll = <1104>;
		dev_pll = <660>;
		display_pll = <1056>;
		//ddr_pll_spread_spectrum =<1>; /* 0 : disable ; else enable*/
		//nand_pll_spread_spectrum =<1>;  /* 0 : disable ; else enable*/
		//dp_pll_spread_spectrum =<1>; /* 0 : disable ; else enable*/
	};

	pwm: pwm@e01b0000 {
		pwm2 {
			id = <2>;
			mfp = <0>;
		};
	};

	edp@e02e0000 {
		panel_configs = <&config0>;

		/*
		 * edp_configs, used to configure edp controller,
		 * its values should set according to edp panel's spec.
		 * Currently, the configration is for lg-lp097qx1 panel.
		 */
		config0: edp_configs {
			link_rate = <1>;	/* 0, 1.62G; 1, 2.7G; 2, 5.4G */

			lane_count = <4>;
			lane_polarity = <0x10>;	/* bit 0~3 map for data lanes' polarity,
						   bit 4 map for aux lanes' polarity,
						   0 normal, 1 reversed */
			lane_mirror = <0>;	/* lane mirror, 0 lane0~3, 1 lane3~0 */

			mstream_polarity = <0x3>;/* bit map for mstream's polarity,
						   bit1: vsync, bit0 hsync */
			user_sync_polarity = <0xc>;/* bit map for user sync's polarity,
						    * bit3: sync signal polarity,
						    * bit2: enable signal polarity,
						    * bit1: vsync polarity,
						    * bit0: hsync polarity
						    */

			pclk_parent = <0>;	/* 0, ASSIST_PLL, 1, DISPLAY PLL */
			pclk_rate = <250000000>;/* 250MHz, ASSIST_PLL, divider 2 */
		};

		/* sharp-tc358860 edp2dsi panel */
		panel_tc358860: edp_panel {
			compatible = "actions,panel-tc358860";

			/* panel's fixed info */
			width_mm = <68>;
			height_mm = <121>;
			bpp = <24>;

			is_primary = <1>;
			hotplugable = <0>;
			hotplug_always_on = <0>;

			/* operation delay in ms */
			power_on_delay = <10>;
			power_off_delay = <0>;
			enable_delay = <10>;
			disable_delay = <0>;

			power-gpio = <&gpioa 18 0>; /* GPIOA18, active high */
			power1-gpio = <&gpioa 19 0>; /* GPIOA19, active high */

			reset-gpio = <&gpioa 20 0>; /* GPIOA20, active high */
			reset1-gpio = <&gpioa 6 1>; /* GPIOA20, low high */

			videomode-0 = <&mode0>;
			/* 1440x2560p60 */
			mode0: videomode {
				refresh_rate = <60>;

				xres = <1440>;
				yres = <2560>;

				/*
				 * pclk_rate = 200MHz, ASSIS_PLL, divider 2.5
				 * pixel_clock = picoseconds / pclk_rate
				 */
				pixel_clock = <5000>;

				hsw = <32>;
				hbp = <80>;
				hfp = <80>;

				vsw = <2>;
				vbp = <6>;
				vfp = <8>;

				/* 0: FB_VMODE_NONINTERLACED, 1:FB_VMODE_INTERLACED */
				vmode = <0>;
			};
		};
	};

	i2c4: i2c@e0178000 {
		edp_i2c {
			compatible = "actions,edp-i2c-init";
			reg = <0x0e>;
			u-boot,i2c-offset-len = <2>;
		};
	};

	i2c3: i2c@e0176000 {
		atc2603c_pmic {
			compatible = "actions,atc2603c";
			reg = <0x65>;

			atc2603c_rtc {
					compatible = "actions,atc2603c-rtc";
			};

			atc2603c_pstore {
					compatible = "actions,atc2603c-pstore";
			};

			atc2603c_misc {
					compatible = "actions,atc2603c-misc";
			};

			atc2603c_auxadc {
					compatible = "actions,atc2603c-auxadc";
			};

			voltage-regulators {

			/*
			 * DCDC1       VDD_CORE
			 * DCDC2       VDDR
			 * DCDC3       VCC
			 */

			dcdc1: dcdc1 {
				regulator-name = "dcdc1";
				regulator-min-microvolt = <925000>;
				regulator-max-microvolt = <1000000>;
				regulator-always-on;
			};

			dcdc3: dcdc3 {
				regulator-name = "dcdc3";
				regulator-min-microvolt = <3100000>;
				regulator-max-microvolt = <3100000>;
				regulator-always-on;
			};

			/*
			 * LDO1        AVCC_3V1
			 * LDO2        SENS_AVDD
			 * LDO3        ATC2603C_VDD
			 *
			 * LDO5        TPVCC3V1
			 * LDO6        AVDD_1V0
			 * LDO7        VDDR (1.8V)
			 *
			 * LDO9        RTC_VDD
			 * SWITCH LDO1 SDCARD_VCC
			 */

			ldo1: ldo1{
				regulator-name = "ldo1";
				regulator-min-microvolt  = <3100000>;
				regulator-max-microvolt = <3100000>;
				regulator-always-on;
			};

			ldo5: ldo5{
				regulator-name = "ldo5";
				regulator-min-microvolt  = <3100000>;
				regulator-max-microvolt = <3100000>;
				regulator-always-on;
			};

			ldo6: ldo6{
				regulator-name = "ldo6";
				regulator-min-microvolt  = <1100000>;
				regulator-max-microvolt = <1100000>;
				regulator-always-on;
			};

			ldo7: ldo7{
				regulator-name = "ldo7";
				regulator-min-microvolt  = <1800000>;
				regulator-max-microvolt = <1800000>;
				regulator-always-on;
			};

			};
		};

		atc2603c_adckeypad {
			#define KEY_VOLUMEDOWN	114
			#define KEY_VOLUMEUP	115
			compatible = "actions,atc2603c-adckeypad";
			keymapsize = <2>;
			adc_channel_name = "AUX0";
			key_val = <KEY_VOLUMEUP KEY_VOLUMEDOWN>;
			left_adc_val =  <0 800>;
			right_adc_val = <100 980>;
		};

		atc2603c-battery{
			compatible = "actions,atc2603c-battery";
			capacity = <7000>;/*unit:mAh*/
			shutdown_current = <50>;/*unit:ua*/
		};
		atc2603c-charger{
			compatible = "actions,atc2603c-charger";
			rsense = <10>;/*unit:mohm*/
			support_adaptor_type = <2>; /*1: DCIN  2: USB  3:DCIN+USB*/
			usb_pc_ctl_mode = <1>; /*0:disable vbus ctl;1:current limited;2:voltage limited*/

		};
	};

	i2c5: i2c@e017a000 {
		bluewhale-charger {
			compatible = "o2micro,bluewhale-charger";
			vbus_ilimt = <500>;
		};
	};

	backlight {
		compatible = "actions,s900-pwm-backlight";

		/* GPIOA14, active high */
		en-gpios = <&gpioa 14 0>;

		/*1.pwm num; 2. period in ns; */
		/*3.plarity, 0: high active, 1: low active*/
		pwms = <&pwm 2 50000 0>;

		total_steps = <1024>;
		min_brightness = <100>;
		max_brightness = <1000>;
		dft_brightness = <500>;

		delay_bf_pwm = <0>; /*in ms*/
		delay_af_pwm = <5>; /*in ms*/
	};
	xhci@e0400000 {
		actions,vbus-gpio = <&gpiod 1 0>;  /* GPIOD1,  0:  high active ; 1: low active */
	};
};
