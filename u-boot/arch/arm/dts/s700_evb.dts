/*
 * Copyright (c) 2015 Actions Semi Co., Ltd.
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

/dts-v1/;

#include "s700.dtsi"

/ {
	model = "s700 upgrade";
	compatible = "actions,s700_upgrade", "actions,s700";

	chosen {
		stdout-path = &serial5;
	};

	clock:e0168000 {
		compatible = "actions,owl-clk";
		core_pll = <600>;
		dev_pll = <600>;
		display_pll = <720>;
	};

	pwm: pwm@e01b0000 {
		pwm0 {
			id = <0>;
			mfp = <0>;
		};

		pwm1 {
			id = <1>;
			mfp = <0>;
		};

		pwm2 {
			id = <2>;
			mfp = <0>;
		};
	};

	xhci@e0400000 {
		actions,vbus-gpio = <&gpioc 14 0>;  /* GPIOC14,  1: low active; 0: high active */
	};

	i2c0: i2c@e0170000 {
		#address-cells = <1>;
		#size-cells = <1>;
		atc2603c_pmic {
			compatible = "actions,atc2603c";
			reg = <0x65 1>;


			total_steps = <2625>;
			pwms_gpu = <&pwm 1 2625 0>;
			vdd_gpu = <485>;
			vdd_core = <490>;
			pwms_core = <&pwm 2 2625 0>;
			atc2603c_pstore {
				compatible = "actions,atc2603c-pstore";
			};

			voltage-regulators {
				dcdc1: dcdc1 {
					regulator-name = "dcdc1";
					regulator-min-microvolt = <1150000>;
					regulator-max-microvolt = <1150000>;
					regulator-always-on;
				};

				dcdc3: dcdc3 {
					regulator-name = "dcdc3";
					regulator-min-microvolt = <3100000>;
					regulator-max-microvolt = <3100000>;
					regulator-always-on;
				};

				ldo1: ldo1{
					regulator-name = "ldo1";
					regulator-min-microvolt  = <2700000>;
					regulator-max-microvolt = <2700000>;
					regulator-always-on;
				};


			ldo7: ldo7{
				regulator-name = "ldo7";
				regulator-min-microvolt  = <1800000>;
				regulator-max-microvolt = <1800000>;
				regulator-always-on;
			};
		};
		atc2603c_irkey {
			compatible = "actions,atc2603c-ir";
			config {
				protocol = <1>; /* 0:9012 1:NEC8 2:RC5 */
				customer_code = <0x0dff40>;
				wk_code = <0x4d>;
				keymapsize = <45>;
				ir_code  =<0x0 0x01 0x02 0x03 0x04 0x05 0x06 0x07 0x08 0x09  0x0A  0x0B  0x0C  0x0D  0x0E  0x0F  0x10 0x11  0x12 0x13 0x14 0x15  0x16 0x17 0x18 0x19 0x1A  0x1B 0x1C  0x1D 0x1E 0x1F 0x40 0x41 0x42  0x43  0x44 0x45  0x46 0x47 0x4D  0x53 0x54  0x57 0x5B>;
				key_code =<11   2   3    4    5    6    7    8    9 	 10 	 100    103  126   353   108   52    105  106    14   60   63  115    80   61   65   76   172   78   114   97   79   75   77  81   158    113  65   139   64   71   116   72   175   82   73>;
			};
			config-1 {
				protocol = <1>; /* 0:9012 1:NEC8 2:RC5 */
				customer_code = <0x0dff40>;
				wk_code = <0x4d>;
				keymapsize = <45>;
				ir_code  =<0x0 0x01 0x02 0x03 0x04 0x05 0x06 0x07 0x08 0x09  0x0A  0x0B  0x0C  0x0D  0x0E  0x0F  0x10 0x11  0x12 0x13 0x14 0x15  0x16 0x17 0x18 0x19 0x1A  0x1B 0x1C  0x1D 0x1E 0x1F 0x40 0x41 0x42  0x43  0x44 0x45  0x46 0x47 0x4D  0x53 0x54  0x57 0x5B>;
				key_code =<11   2   3    4    5    6    7    8    9 	 10 	 100    103  126   353   108   52    105  106    14   60   63  115    80   61   65   76   172   78   114   97   79   75   77  81   158    113  65   139   64   71   116   72   175   82   73>;
			};
		};

		};
	};

	i2c3: i2c@e017c000 {
		hdmi_edid {
			compatible = "actions,hdmi-edid";
		};
	};

	backlight {
		compatible = "actions,s700-pwm-backlight";

		/* GPIOD28, active high */
		en-gpios = <&gpioa 23 1>;

		/*1.pwm num; 2. period in ns; */
		/*3.plarity, 0: high active, 1: low active*/
		pwms = <&pwm 0 50000 1>;

		total_steps = <1024>;
		min_brightness = <100>;
		max_brightness = <1000>;
		dft_brightness = <500>;

		delay_bf_pwm = <0>; /*in ms*/
		delay_af_pwm = <20>; /*in ms*/
	};

	lcd@e02a0000 {
		panel_configs = <&config0>;

		/* 1280x800p60 */
		config0: lcd_configs {
			port_type = <2>;	/* 0, RGB; 1, CPU; 2, LVDS; 3, EDP */

			vsync_inversion = <0>;
			hsync_inversion = <0>;
			dclk_inversion = <1>;
			lde_inversion = <0>;

			pclk_parent = <0>;	/* 0, DISPLAY PLL; 1, NAND PLL */
			pclk_rate = <66000000>;/* 66MHz, DISPLAY PLL, divider 16 */
			};
		/* lcd panel */
		panel@glp {
			compatible = "actions,panel-glp";

			/* panel's fixed info */
			width_mm = <197>;
			height_mm = <147>;
			bpp = <24>;

			is_primary = <1>;

			/* operation delay in ms */
			power_on_delay = <0>;
			power_off_delay = <0>;
			enable_delay = <0>;
			disable_delay = <0>;

			power-gpio = <&gpiob 4 0>;/* GPIOD30, high power on */

			videomode-0 = <&mode0>;
			/* 800 * 1280p60 */
			mode0: videomode {
				refresh_rate = <60>;
				xres = <800>;
				yres = <1280>;
				/*
				 * pclk_rate = 66M
				 * pixel_clock = picoseconds / pclk_rate
				 */
				pixel_clock = <15152>;

				hsw = <12>;
				hbp = <20>;
				hfp = <40>;

				vsw = <2>;
				vbp = <10>;
				vfp = <33>;

				/* 0: FB_VMODE_NONINTERLACED, 1:FB_VMODE_INTERLACED */
				vmode = <0>;
			};
		};
	};

	hdmi@e02c0000 {
		channel_invert = <0>;
		bit_invert = <0>;

		panel@ghp {
			compatible = "actions,panel-ghp";

			is_primary = <0>;
		};
	};
};
