/*
 * Copyright (c) 2015 Actions Semi Co., Ltd.
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#ifndef __ATC260X_ADCKEY_H
#define __ATC260X_ADCKEY_H

int atc260x_adckey_scan(void);

#endif /* __ATC260X_ADCKEY_H */
