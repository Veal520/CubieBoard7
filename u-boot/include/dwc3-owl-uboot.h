/*
 * Actions OWL SoCs dwc3 driver
 *
 * Copyright (c) 2015 Actions Semiconductor Co., Ltd.
 * tangshaoqing <tangshaoqing@actions-semi.com>
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#ifndef __DWC3_OWL_UBOOT_H_
#define __DWC3_OWL_UBOOT_H_

int dwc3_owl_uboot_init(void);
int dwc3_owl_uboot_exit(void);

#endif /* __DWC3_OWL_UBOOT_H_ */
