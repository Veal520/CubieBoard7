===Build Steps===

0)  == Check md5sum ==
$ md5sum  android5.1_sdk_s700_v1.2.tar.gz 

1) == Tar source ==
$ mkdir  android5.1_sdk_s700_v1.2
$ tar -zxpf  android5.1_sdk_s700_v1.2.tar.gz  -C android5.1_sdk_s700_v1.2
$ cd android5.1_sdk_s700_v1.2 
$ git reset --hard 
$ git checkout sdk_v1.2

2) == Chose "s700_cb7 or s700_aio" to build ==
$ cd android5.1_sdk_s700_v1.2
$ ./autobuild.sh config // chose product 
 
#Select board type:
#     1. s700_aio
#     2. s700_cb7
$ ./autobuild.sh // start to build 

3) == Get image== 
owl/out/s700_android_s700_cb7/images/s700_android_s700_*.fw
