LOCAL_PATH := $(call my-dir)

#ifneq ($(BOARD_HAVE_BLUETOOTH_RTK),)
ifeq ($(strip $(R_BT_TYPE)), ap6212)

include $(CLEAR_VARS)

BDROID_DIR := $(TOP_DIR)external/bluetooth/bluedroid

#ifeq ($(R_WIRELESS_SLEEP),true)
#$(info "R_WIRELESS_SLEEP define BT_USE_LPM_SLEEP")
LOCAL_CFLAGS += -DBT_USE_LPM_SLEEP
#endif

LOCAL_SRC_FILES := \
        src/bt_vendor_brcm.c \
        src/hardware.c \
        src/userial_vendor.c \
        src/upio.c \
        src/conf.c

LOCAL_C_INCLUDES += \
        $(LOCAL_PATH)/include \
        $(BDROID_DIR)/hci/include

LOCAL_SHARED_LIBRARIES := \
        libcutils \
        liblog

LOCAL_MODULE := libbt-vendor
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_MODULE_OWNER := broadcom
#LOCAL_MODULE_PATH := $(TARGET_OUT_VENDOR_SHARED_LIBRARIES)
#LOCAL_PROPRIETARY_MODULE := true

#include $(LOCAL_PATH)/vnd_buildcfg.mk

ifneq ($(strip $(R_BT_USE_UART)), )
$(info R_BT_USE_UART=$(R_BT_USE_UART))

ret=$(shell grep "$(R_BT_USE_UART)" $(LOCAL_PATH)/include/vnd_buildcfg.h)
ifeq ($(strip $(ret)), )
$(info replace bt USE_UART with $(R_BT_USE_UART) in $(LOCAL_PATH)/include/vnd_buildcfg.h)
$(shell sed -i "s/ttyS[0-9]/$(R_BT_USE_UART)/g" $(LOCAL_PATH)/include/vnd_buildcfg.h)
endif

else
$(info replace bt USE_UART with ttyS3 in $(LOCAL_PATH)/include/vnd_buildcfg.h)
$(shell sed -i "s/ttyS[0-9]/ttyS3/g" $(LOCAL_PATH)/include/vnd_buildcfg.h)
endif


include $(BUILD_SHARED_LIBRARY)

endif # BOARD_HAVE_BLUETOOTH_RTK
