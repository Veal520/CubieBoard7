LOCAL_PATH:= $(call my-dir)



common_CFLAGS := -W -g -DPLATFORM_ANDROID
common_C_INCLUDES +=  \
   $(TARGET_OUT_HEADERS)/drm \
   $(TARGET_OUT_HEADERS)/libdrm \
   $(TARGET_OUT_HEADERS)/libdrm/shared-core

include $(CLEAR_VARS)

common_SRC_FILES := \
	libdrm/libdrm_lists.h \
	libdrm/xf86drm.c \
	libdrm/xf86drmHash.c \
	libdrm/xf86drmRandom.c \
	libdrm/xf86drmMode.c \
	libdrm/xf86drmSL.c \


ifeq ($(TARGET_ARCH),arm)
	LOCAL_CFLAGS += -fstrict-aliasing -fomit-frame-pointer
endif

LOCAL_CFLAGS += $(common_CFLAGS)
ifeq ($(TARGET_OS)-$(TARGET_ARCH),linux-x86)
LOCAL_CFLAGS += -DUSTL_ANDROID_X86
endif

LOCAL_SRC_FILES := $(common_SRC_FILES)
LOCAL_C_INCLUDES += $(common_C_INCLUDES)

LOCAL_MODULE_TAGS :=eng optional
LOCAL_MODULE := libdrm
include $(BUILD_SHARED_LIBRARY)


