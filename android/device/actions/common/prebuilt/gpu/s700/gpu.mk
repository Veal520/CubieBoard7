# vendor/lib/egl
PRODUCT_COPY_FILES += \
$(LOCAL_PATH)/vendor/lib/egl/libGLES_mali.so:system/vendor/lib/egl/libGLES_mali.so

# vendor/lib/hw
PRODUCT_COPY_FILES += \
$(LOCAL_PATH)/vendor/lib/hw/gralloc.S700.so:system/vendor/lib/hw/gralloc.S700.so

# vendor/lib/hw
PRODUCT_COPY_FILES += \
$(LOCAL_PATH)/vendor/lib/hw/hwcomposer.S700.so:system/vendor/lib/hw/hwcomposer.S700.so

# vendor/lib64/egl
#PRODUCT_COPY_FILES += \
#$(LOCAL_PATH)/vendor/lib64/egl/libGLES_mali.so:system/vendor/lib64/egl/libGLES_mali.so

# vendor/lib64/hw
#PRODUCT_COPY_FILES += \
#$(LOCAL_PATH)/vendor/lib64/hw/gralloc.s700.so:system/vendor/lib64/hw/gralloc.default.so

# egl.cfg
PRODUCT_COPY_FILES += \
$(LOCAL_PATH)/egl.cfg:system/lib/egl/egl.cfg
