package com.actions.music.data;

import android.database.Cursor;
import android.net.Uri;

import com.actions.mediacenter.data.MediaObject;
import com.actions.mediacenter.data.Path;

public class LocalMusic extends MediaObject {
	private Path path;
	private String artist;
	private String album;
	private String title;
	private int id;
	private int duration;
	
	public static final String COLUMN_KEY_ID = "_id";
	public static final String COLUMN_KEY_DATA = "_data";
	public static final String COLUMN_KEY_TITLE = "title";
	public static final String COLUMN_KEY_ARTIST = "artist";
	public static final String COLUMN_KEY_ALBUM = "album";
	public static final String COLUMN_KEY_DURATION = "duration";
	
	public static final int COLUMN_INDEX_ID = 0;
	public static final int COLUMN_INDEX_DATA = 1;
	public static final int COLUMN_INDEX_TITLE = 2;
	public static final int COLUMN_INDEX_ARTIST = 3;
	public static final int COLUMN_INDEX_ALBUM = 4;
	public static final int COLUMN_INDEX_DURATION = 5;
	
	public static final String[] PROJECTIONS = {COLUMN_KEY_ID, COLUMN_KEY_DATA, COLUMN_KEY_TITLE, COLUMN_KEY_ARTIST, COLUMN_KEY_ALBUM, COLUMN_KEY_DURATION};
	
	public LocalMusic() {
		
	}
	
	public LocalMusic(Cursor cursor){
		if(cursor!=null && !cursor.isClosed()){
			path = new Path(cursor.getString(COLUMN_INDEX_DATA));
			id = cursor.getInt(COLUMN_INDEX_ID);
			title = cursor.getString(COLUMN_INDEX_TITLE);
			artist = cursor.getString(COLUMN_INDEX_ARTIST);
			album = cursor.getString(COLUMN_INDEX_ALBUM);
			duration = cursor.getInt(COLUMN_INDEX_DURATION);
		}
	}
	
	@Override
	public String getDetails() {
		return null;
	}

	@Override
	public Uri getUri() {
		return null;
	}

	@Override
	public Path getMediaPath() {
		return path;
	}
	
	public String getName(){
		return path.getName();
	}
	
	public String getTitle(){
		return title;
	}
	
	public String getArtist(){
		return artist;
	}
	
	public String getAlbum(){
		return album;
	}
	
	public int getID(){
		return id;
	}
	
	public int getDuration(){
		return duration;
	}

}
