package com.actions.mediacenter.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;

import com.actions.mediacenter.R;

public class BaseActivity extends Activity {
	private AlertDialog mAlertDialog = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		IntentFilter filter = new IntentFilter(Intent.ACTION_MEDIA_MOUNTED);
		filter.addDataScheme("file");
		registerReceiver(mBaseBroadcastReceiver, filter);
	}

	@Override
	protected void onStart() {
		super.onStart();
		checkExternalStorageState();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(mBaseBroadcastReceiver);
	}

	private BroadcastReceiver mBaseBroadcastReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals(Intent.ACTION_MEDIA_MOUNTED)) {
				if (getExternalCacheDir() != null) {
					onStorageReady();
				}
			}
		}
	};

	protected void onStorageReady() {
		if (mAlertDialog != null) {
			mAlertDialog.dismiss();
			mAlertDialog = null;
			updateContent();
		}
	}

	protected void updateContent() {

	}

	private void checkExternalStorageState() {
		if (getExternalCacheDir() == null) {
			OnCancelListener onCancel = new OnCancelListener() {
				@Override
				public void onCancel(DialogInterface dialog) {
					finish();
				}
			};
			DialogInterface.OnClickListener onClick = new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.cancel();
				}
			};

			AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.HoloSmallDialog).setTitle(R.string.no_external_storage_title)
					.setIcon(R.drawable.ic_dialog_alert).setMessage(R.string.no_external_storage).setNegativeButton(android.R.string.cancel, onClick)
					.setOnCancelListener(onCancel);
			mAlertDialog = builder.show();
		}
	}
}
