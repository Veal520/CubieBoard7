package com.actions.image.app;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView.ScaleType;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.actions.image.data.ImageSetManager;
import com.actions.image.data.LocalImage;
import com.actions.image.data.LocalImageSet;
import com.actions.image.gif.AnimationThread;
import com.actions.image.gif.AnimationThread.OnBitmapChangedListener;
import com.actions.image.widget.CustomImageView;
import com.actions.image.widget.GestureRecognizer;
import com.actions.image.widget.ImageOperationControl;
import com.actions.mediacenter.R;
import com.actions.mediacenter.data.Path;
import com.actions.mediacenter.utils.BitmapLoader;
import com.actions.mediacenter.utils.BitmapLoader.ImageCallback;
import com.actions.owlplayer.app.OWLApplication;

public class ImagePlayActivity extends Activity implements AnimationListener {
	private static final String TAG = "ImagePlayActivity";

	private static final int SWIPE_ESCAPE_VELOCITY = 1200; // dp/sec
	private static final int VIEW_SWITCH_INTERVAL = 500; // ms
	private static final int IMAGE_SLIDE_SHOW_GAPS = 3000;
	private static final int ACTION_BAR_SHOW_GAPS = 2000;

	private static final int MESSAGE_INVISIBLE_ACTION_BAR = 1;
	private static final int MESSAGE_SLIDE_SHOW_NEXT = 2;
	private static final int MESSAGE_REFRESH_GIF_IMAGE = 3;

	private Context mContext;
	private Activity mActivity;
	private OWLApplication mApplication;
	private RelativeLayout mActionBar;
	private TextView mTitleTextView;
	private TextView mIndexAndCountTextView;
	private CustomImageView mImageViewAlpha;
	private CustomImageView mImageViewBeta;
	private ViewFlipper mImageViewFlipper;
	private ProgressBar mProgressBar;
	private BitmapLoader mBitmapLoader;
	private DisplayMetrics mDisplayMetrics;
	private float mPixelDensity = -1f;
	private ViewTreeObserver mViewTreeObserver;
	private AlertDialog mAlertDialog = null;

	private Animation mAnimationFadeIn;
	private Animation mAnimationFadeOut;
	private Animation mAnimationLeftIn;
	private Animation mAnimationLeftOut;
	private Animation mAnimationSlideFadeIn;
	private Animation mAnimationSlideLeftOut;
	private boolean isAnimationRunning = false;
	private long mViewSwitchStart = 0;
	private boolean mDecodeFinished = true;

	private int mScreenWidth = 0;
	private int mScreenHeight = 0;

	private boolean mCurrentIsAlpha = false;
	private Matrix mMatrix = new Matrix();
	private float mCurrentDegree = 0f;

	private LocalImage mCurrentItem;
	private int mCurrentIndex;
	private boolean isSlideShowing = false;

	private MyGestureListener mGestureListener;
	private GestureRecognizer mGestureRecognizer;

	private AnimationThread mAnimationThread;
	private Bitmap mGifBitmap;
	private CustomImageView mCurrentImageView;
	private Matrix mCurrentMatrix;
	private int mImageSetPosition;
	private LocalImageSet mImageSet;
	private ImageSetManager mImageSetManager;
	private View mRootView;
	private ImageOperationControl mImageOperationControl;
	private OnGlobalLayoutListener mOnGlobalLayoutListener = new OnGlobalLayoutListener() {
		@Override
		public void onGlobalLayout() {
			setImageViewRange();
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
		setContentView(R.layout.image_play_layout);

		mRootView = findViewById(android.R.id.content);
		mContext = this;
		mActivity = this;
		mImageSetManager = ImageSetManager.getInstance(mContext);
		mApplication = (OWLApplication) getApplication();

		if (savedInstanceState == null) {
			mCurrentIndex = getIntent().getIntExtra(Config.INTENT_KEY_IMAGE_ITEM_POSITION, -1);
			mImageSetPosition = getIntent().getIntExtra(Config.INTENT_KEY_IMAGE_SET_POSITION, -1);
		} else {
			mCurrentIndex = savedInstanceState.getInt(Config.INTENT_KEY_IMAGE_ITEM_POSITION);
			mImageSetPosition = savedInstanceState.getInt(Config.INTENT_KEY_IMAGE_SET_POSITION);
		}

		if (getExternalCacheDir() == null) {
			mCurrentItem = null;
			finish();
			return;
		}

		initBroadcastReceiver();
		initData();
		initView();
	}

	private void initView() {
		mActionBar = (RelativeLayout) findViewById(R.id.image_action_bar);
		mTitleTextView = (TextView) findViewById(R.id.image_title_text);
		mTitleTextView.setOnClickListener(mOnButtonClickListener);
		mIndexAndCountTextView = (TextView) findViewById(R.id.image_index_and_count);

		mImageOperationControl = new ImageOperationControl(mRootView);
		mImageOperationControl.setOnClickListener(mOnButtonClickListener);

		mImageViewFlipper = (ViewFlipper) findViewById(R.id.image_show_flipper);

		mImageViewAlpha = new CustomImageView(mContext);
		mImageViewAlpha.setScaleType(ScaleType.FIT_XY);
		mImageViewAlpha.setmActivity(mActivity);
		mImageViewAlpha.setClickable(false);
		mImageViewAlpha.setFocusable(false);
		mImageViewBeta = new CustomImageView(mContext);
		mImageViewBeta.setScaleType(ScaleType.FIT_XY);
		mImageViewBeta.setmActivity(mActivity);
		mImageViewBeta.setClickable(false);
		mImageViewBeta.setFocusable(false);

		addProgressBar();

		mBitmapLoader = BitmapLoader.getInstance(mApplication);
		mDisplayMetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getRealMetrics(mDisplayMetrics);
		mPixelDensity = mDisplayMetrics.density;
		mScreenWidth = mDisplayMetrics.widthPixels;
		mScreenHeight = mDisplayMetrics.heightPixels;
		setImageViewRange();

		mViewTreeObserver = mImageViewFlipper.getViewTreeObserver();
		mViewTreeObserver.addOnGlobalLayoutListener(mOnGlobalLayoutListener);

		mGestureListener = new MyGestureListener();
		mGestureRecognizer = new GestureRecognizer(mContext, mGestureListener);

		mAnimationFadeIn = AnimationUtils.loadAnimation(this, R.anim.push_fade_in);
		mAnimationFadeOut = AnimationUtils.loadAnimation(this, R.anim.push_fade_out);
		mAnimationLeftIn = AnimationUtils.loadAnimation(this, R.anim.push_left_in);
		mAnimationLeftOut = AnimationUtils.loadAnimation(this, R.anim.push_left_out);
		mAnimationSlideFadeIn = AnimationUtils.loadAnimation(this, R.anim.slide_fade_in);
		mAnimationSlideLeftOut = AnimationUtils.loadAnimation(this, R.anim.slide_left_out);
		mAnimationFadeIn.setAnimationListener(this);
		mAnimationFadeOut.setAnimationListener(this);
		mAnimationLeftIn.setAnimationListener(this);
		mAnimationLeftOut.setAnimationListener(this);
		mAnimationSlideFadeIn.setAnimationListener(this);
		mAnimationSlideLeftOut.setAnimationListener(this);
	}

	public void initData() {
		if (getIntent().getData() != null) {
			mCurrentIndex = 0;
			String filePath = getIntent().getData().getPath();
			Log.d(TAG, "filePath:"+filePath);
			mImageSet = new LocalImageSet(new Path(filePath.substring(0, filePath.lastIndexOf("/"))));
			mCurrentItem = new LocalImage(mContext);
			mCurrentItem.data = filePath;
			mImageSet.addItem(mCurrentItem);
			return;
		}
		mImageSet = mImageSetManager.get(mImageSetPosition);
		mCurrentItem = (LocalImage) mImageSet.getItem(mCurrentIndex);
	}

	@Override
	public void onResume() {
		super.onResume();
		if (mCurrentItem != null) {
			initViewContent();
			hideControlLayout();
			if (isInTouchMode()) {
				showControlLayout();
			}
		}
	}

	@Override
	public void onPause() {
		super.onPause();

		if (mAlertDialog != null) {
			mAlertDialog.dismiss();
			mAlertDialog = null;
		}

		keepScreenOn(false);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.v(TAG, "onDestroy!!!!!");
		releaseBroadcastReceiver();
		stopGifAnimation();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(Config.INTENT_KEY_IMAGE_ITEM_POSITION, mImageSetPosition);
		outState.putInt(Config.INTENT_KEY_IMAGE_ITEM_POSITION, mCurrentIndex);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		Log.v(TAG, "onKeyDown");
		switch (keyCode) {
		case KeyEvent.KEYCODE_BACK:
			if (isSlideShowing) {
				toggleSlideShow(false);
				showControlLayout();
			} else {
				finish();
			}
			return true;

		case KeyEvent.KEYCODE_MENU:
			if (isSlideShowing) {
				toggleSlideShow(false);
			}
			toggleControlLayout();
			return true;

		case KeyEvent.KEYCODE_DPAD_CENTER:
		case KeyEvent.KEYCODE_ENTER:
			if (isSlideShowing) {
				toggleSlideShow(false);
			}
			showActionBar(true);
			break;

		case KeyEvent.KEYCODE_DPAD_UP:
			if (isSlideShowing) {
				toggleSlideShow(false);
			}
			previous();
			break;

		case KeyEvent.KEYCODE_DPAD_DOWN:
			if (isSlideShowing) {
				toggleSlideShow(false);
			}
			next();
			break;
		default:
			break;
		}
		return super.onKeyDown(keyCode, event);
	}

	private void setImageViewRange() {
		mImageViewAlpha.setDisplayHeight(mScreenHeight);
		mImageViewAlpha.setDisplayWidth(mScreenWidth);
		mImageViewBeta.setDisplayHeight(mScreenHeight);
		mImageViewBeta.setDisplayWidth(mScreenWidth);
	}

	private OnBitmapChangedListener mOnBitmapChangedListener = new OnBitmapChangedListener() {

		@Override
		public void onBitmapChanged(Bitmap bitmap) {
			Message msg = mHandler.obtainMessage(MESSAGE_REFRESH_GIF_IMAGE, bitmap);
			mHandler.sendMessage(msg);
		}

	};

	private void stopGifAnimation() {
		if (mAnimationThread != null) {
			mHandler.removeMessages(MESSAGE_REFRESH_GIF_IMAGE);
			mAnimationThread.stop();
			if (mGifBitmap != null && !mGifBitmap.isRecycled()) {
				mCurrentImageView.setImageBitmap(null);
				mGifBitmap.recycle();
			}
		}
	}

	private void addImageView(String path, final CustomImageView view, final float rotate) {
		Log.v(TAG, "addImageView " + mCurrentItem.data);
		view.setTag(path);

		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inPreferredConfig = Bitmap.Config.ARGB_8888;
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(path, options);
		Rect rect = view.getShowRange(options.outWidth, options.outHeight);

		mCurrentDegree += rotate;
		mCurrentMatrix = new Matrix();
		if (mCurrentMatrix != null && mCurrentDegree % 360 != 0) {
			int centerX = view.getLeft() + (view.getWidth() / 2);
			int centerY = view.getTop() + (view.getHeight() / 2);
			mCurrentMatrix.postRotate(mCurrentDegree, centerX, centerY);
			view.setImageMatrix(mCurrentMatrix);
		}
		mDecodeFinished = false;

		Bitmap bitmap = null;
		if (isGifImage() && !isSlideShowing) {
			if (mAnimationThread == null) {
				mAnimationThread = new AnimationThread();
				mAnimationThread.setOnBitmapChangedListener(mOnBitmapChangedListener);
			}
			mHandler.removeMessages(MESSAGE_REFRESH_GIF_IMAGE);
			mAnimationThread.stop();
			mAnimationThread.setGifImage(mCurrentItem.data);
			mAnimationThread.start();
		} else {
			stopGifAnimation();
			bitmap = mBitmapLoader.loadBitmap(path, BitmapLoader.REQUEST_THUMB_FULL, new ImageCallback() {
				@Override
				public void imageLoad(final String tag, Bitmap bitmap) {
					if (isSlideShowing) {
						mHandler.sendEmptyMessageDelayed(MESSAGE_SLIDE_SHOW_NEXT, IMAGE_SLIDE_SHOW_GAPS);
					}
					mDecodeFinished = true;
					showProgress(View.INVISIBLE);
					if (bitmap == null) {
						showLoadFailed();
					} else {
						if (mCurrentDegree % 360 != 0) {
							bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), mCurrentMatrix, true);
						}
					}
					String imageTag = (String) view.getTag();
					if (tag.equals(imageTag)) {
						view.setImageBitmap(bitmap);
					}
				}
			});
		}

		if (bitmap != null && !bitmap.isRecycled()) {
			mDecodeFinished = true;
			showProgress(View.INVISIBLE);
			if (mCurrentDegree % 360 != 0) {
				bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), mCurrentMatrix, true);
			}
			view.setImageBitmap(bitmap);
		} else {
			view.setImageBitmap(null);
		}

		if (mCurrentDegree % 180 != 0) {
			rect = view.getShowRange(options.outHeight, options.outWidth);
		}

		if (mImageViewFlipper.getCurrentView() != view) {
			FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(rect.width(), rect.height());
			params.gravity = Gravity.CENTER;
			mImageViewFlipper.addView(view, -1, params);
		}
		mCurrentImageView = view;
	}

	private void addProgressBar() {
		if (mProgressBar == null) {
			mProgressBar = new ProgressBar(this);
			mProgressBar.setVisibility(View.GONE);
			FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
			params.gravity = Gravity.CENTER;
			addContentView(mProgressBar, params);
		}
	}

	private void showProgress(int visibility) {
		mProgressBar.setVisibility(visibility);
	}

	private void showLoadFailed() {
		Toast.makeText(mContext, R.string.message_failed_to_load_image, Toast.LENGTH_SHORT).show();
	}

	private void keepScreenOn(boolean screenOn) {
		if (screenOn) {
			getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		} else {
			getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		}
	}

	private void initViewContent() {
		mTitleTextView.setText(mCurrentItem.displayName);
		mIndexAndCountTextView.setText((mCurrentIndex + 1) + "/" + mImageSet.size());
		mDecodeFinished = false;
		showProgress(View.VISIBLE);
		CustomImageView view = mCurrentIsAlpha ? mImageViewAlpha : mImageViewBeta;
		addImageView(mCurrentItem.data, view, 0);
	}

	private Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MESSAGE_INVISIBLE_ACTION_BAR:
				mActionBar.setVisibility(View.INVISIBLE);
				break;
			case MESSAGE_SLIDE_SHOW_NEXT:
				next();
				break;
			case MESSAGE_REFRESH_GIF_IMAGE:
				Bitmap bitmap = (Bitmap) msg.obj;
				mDecodeFinished = true;

				if (mGifBitmap != null && !mGifBitmap.isRecycled()) {
					mGifBitmap.recycle();
					mGifBitmap = null;
				}

				if (bitmap == null || bitmap.isRecycled()) {
					break;
				}

				mGifBitmap = bitmap.copy(bitmap.getConfig(), false);
				if (mCurrentDegree % 360 != 0) {
					mGifBitmap = Bitmap.createBitmap(mGifBitmap, 0, 0, mGifBitmap.getWidth(), mGifBitmap.getHeight(), mCurrentMatrix, true);
				}

				showProgress(View.INVISIBLE);
				mCurrentImageView.setImageBitmap(mGifBitmap);
				break;
			default:
				break;
			}
		}
	};

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		mGestureRecognizer.onTouchEvent(event);
		return true;
	}

	private class MyGestureListener implements GestureRecognizer.Listener {
		@Override
		public boolean onSingleTapUp(float x, float y) {
			if (isSlideShowing) {
				toggleSlideShow(false);
			}
			return true;
		}

		@Override
		public boolean onSingleTapConfirmed(float x, float y) {
			mImageOperationControl.toggleOperationLayout();
			return true;
		}

		@Override
		public boolean onDoubleTap(float x, float y) {
			CustomImageView view = mCurrentIsAlpha ? mImageViewAlpha : mImageViewBeta;
			view.onDoubleTap(x, y);
			return true;
		}

		@Override
		public boolean onScroll(float dx, float dy, float totalX, float totalY) {
			Log.d(TAG, "onScroll");
			CustomImageView view = mCurrentIsAlpha ? mImageViewAlpha : mImageViewBeta;
			view.onScroll(dx, dy, totalX, totalY);
			return true;
		}

		@Override
		public boolean onFling(float velocityX, float velocityY) {
			Log.v(TAG, "onFling");
			int escapeVelocity = Math.round(mPixelDensity * SWIPE_ESCAPE_VELOCITY);
			boolean fastEnough = (Math.abs(velocityX) > escapeVelocity);
			if (fastEnough) {
				if (isAnimationRunning) {
					mAnimationFadeIn.cancel();
					mAnimationLeftOut.cancel();
					mAnimationLeftIn.cancel();
					mAnimationFadeOut.cancel();
					mAnimationSlideFadeIn.cancel();
					mAnimationSlideLeftOut.cancel();
				}

				if (System.currentTimeMillis() - mViewSwitchStart <= VIEW_SWITCH_INTERVAL) {
					Log.e(TAG, "ignore on fling!");
					return true;
				}

				mViewSwitchStart = System.currentTimeMillis();

				if (velocityX >= 0) {
					previous();
				} else {
					next();
				}
			}

			return true;
		}

		@Override
		public boolean onScaleBegin(float focusX, float focusY) {
			Log.v(TAG, "onScaleBegin");
			CustomImageView view = mCurrentIsAlpha ? mImageViewAlpha : mImageViewBeta;
			view.onScaleBegin(focusX, focusY);
			return true;
		}

		@Override
		public boolean onScale(float focusX, float focusY, float scale) {
			Log.v(TAG, "onScale");
			CustomImageView view = mCurrentIsAlpha ? mImageViewAlpha : mImageViewBeta;
			view.onScale(focusX, focusY, scale);

			return true;
		}

		@Override
		public void onScaleEnd() {
			Log.v(TAG, "onScaleEnd");
			CustomImageView view = mCurrentIsAlpha ? mImageViewAlpha : mImageViewBeta;
			view.onScaleEnd();
		}

		@Override
		public void onDown(float x, float y) {

		}

		@Override
		public void onUp() {

		}
	}

	protected void center(boolean horizontal, boolean vertical, int bmWidth, int bmHeight) {

		Matrix m = new Matrix();
		m.set(mMatrix);
		RectF rect = new RectF(0, 0, bmWidth, bmHeight);
		m.mapRect(rect);

		float height = rect.height();
		float width = rect.width();

		float deltaX = 0, deltaY = 0;

		if (vertical) {
			int screenHeight = mDisplayMetrics.heightPixels;
			if (height < screenHeight) {
				deltaY = (screenHeight - height) / 2 - rect.top;
			} else if (rect.top > 0) {
				deltaY = -rect.top;
			} else if (rect.bottom < screenHeight) {
				deltaY = bmHeight - rect.bottom;
			}
		}

		if (horizontal) {
			int screenWidth = mDisplayMetrics.widthPixels;
			if (width < screenWidth) {
				deltaX = (screenWidth - width) / 2 - rect.left;
			} else if (rect.left > 0) {
				deltaX = -rect.left;
			} else if (rect.right < screenWidth) {
				deltaX = screenWidth - rect.right;
			}
		}
		mMatrix.postTranslate(deltaX, deltaY);
	}

	private AnimationListener mFlipAnimListener = new AnimationListener() {
		public void onAnimationStart(Animation animation) {
		}

		public void onAnimationRepeat(Animation animation) {
		}

		public void onAnimationEnd(Animation animation) {
			if (!mDecodeFinished) {
				showProgress(View.VISIBLE);
			}
		}
	};

	private void showActionBar(boolean show) {
		if (show) {
			mActionBar.setVisibility(View.VISIBLE);
			mHandler.sendEmptyMessageDelayed(MESSAGE_INVISIBLE_ACTION_BAR, ACTION_BAR_SHOW_GAPS);
		} else {
			mActionBar.setVisibility(View.INVISIBLE);
		}
	}

	public void toggleSlideShow(boolean slide) {
		Log.v(TAG, "toggleSlideShow: " + slide);
		if (slide) {
			stopGifAnimation();
			mHandler.sendEmptyMessageDelayed(MESSAGE_SLIDE_SHOW_NEXT, IMAGE_SLIDE_SHOW_GAPS);
			mImageOperationControl.hide(1000);
		} else {
			mHandler.removeMessages(MESSAGE_SLIDE_SHOW_NEXT);
		}
		mImageOperationControl.updateSlideShowButton(slide);
		isSlideShowing = slide;
		keepScreenOn(slide);
	}

	private void previous() {
		Log.v(TAG, "previous");
		if (mCurrentIndex <= 0) {
			return;
		}
		mCurrentIndex--;

		if (mImageSet.getItem(mCurrentIndex) == null) {
			if (isSlideShowing) {
				toggleSlideShow(false);
			}
			return;
		}

		mCurrentDegree = 0f;
		CustomImageView current = mCurrentIsAlpha ? mImageViewAlpha : mImageViewBeta;
		CustomImageView switchTo = !mCurrentIsAlpha ? mImageViewAlpha : mImageViewBeta;

		mCurrentItem = (LocalImage) mImageSet.getItem(mCurrentIndex);
		mTitleTextView.setText(mCurrentItem.displayName);
		mIndexAndCountTextView.setText((mCurrentIndex + 1) + "/" + mImageSet.size());
		current.setVisibility(View.INVISIBLE);
		addImageView(mCurrentItem.data, switchTo, 0);
		mImageViewFlipper.setInAnimation(mAnimationLeftIn);
		mImageViewFlipper.setOutAnimation(mAnimationFadeOut);
		mImageViewFlipper.getInAnimation().setAnimationListener(mFlipAnimListener);
		mImageViewFlipper.showPrevious();
		mImageViewFlipper.removeView(current);
		mCurrentIsAlpha = !mCurrentIsAlpha;
	}

	private void next() {
		Log.v(TAG, "next");
		if (mCurrentIndex >= mImageSet.size() - 1) {
			if (isSlideShowing) {
				mCurrentIndex = 0;
			} else {
				return;
			}
		} else {
			mCurrentIndex++;
		}

		if (mImageSet.getItem(mCurrentIndex) == null) {
			if (isSlideShowing) {
				toggleSlideShow(false);
			}
			return;
		}

		mCurrentDegree = 0f;
		CustomImageView current = mCurrentIsAlpha ? mImageViewAlpha : mImageViewBeta;
		CustomImageView switchTo = !mCurrentIsAlpha ? mImageViewAlpha : mImageViewBeta;

		mCurrentItem = (LocalImage) mImageSet.getItem(mCurrentIndex);
		mTitleTextView.setText(mCurrentItem.displayName);
		mIndexAndCountTextView.setText((mCurrentIndex + 1) + "/" + mImageSet.size());
		current.setVisibility(View.INVISIBLE);
		addImageView(mCurrentItem.data, switchTo, 0);
		if (isSlideShowing) {
			mImageViewFlipper.setInAnimation(mAnimationSlideFadeIn);
			mImageViewFlipper.setOutAnimation(mAnimationSlideLeftOut);
		} else {
			mImageViewFlipper.setInAnimation(mAnimationFadeIn);
			mImageViewFlipper.setOutAnimation(mAnimationLeftOut);
		}
		mImageViewFlipper.getInAnimation().setAnimationListener(mFlipAnimListener);
		mImageViewFlipper.showNext();
		mImageViewFlipper.removeView(current);
		mCurrentIsAlpha = !mCurrentIsAlpha;
	}

	private void rotateLeft() {
		CustomImageView current = mCurrentIsAlpha ? mImageViewAlpha : mImageViewBeta;
		CustomImageView switchTo = mCurrentIsAlpha ? mImageViewBeta : mImageViewAlpha;

		addImageView(mCurrentItem.data, switchTo, -90.0f);
		mImageViewFlipper.setInAnimation(null);
		mImageViewFlipper.setOutAnimation(null);
		mImageViewFlipper.showNext();
		mImageViewFlipper.removeView(current);
		mCurrentIsAlpha = !mCurrentIsAlpha;
	}

	private void rotateRight() {
		CustomImageView current = mCurrentIsAlpha ? mImageViewAlpha : mImageViewBeta;
		CustomImageView switchTo = mCurrentIsAlpha ? mImageViewBeta : mImageViewAlpha;

		addImageView(mCurrentItem.data, switchTo, 90.0f);
		mImageViewFlipper.setInAnimation(null);
		mImageViewFlipper.setOutAnimation(null);
		mImageViewFlipper.showNext();
		mImageViewFlipper.removeView(current);
		mCurrentIsAlpha = !mCurrentIsAlpha;
	}

	@Override
	public void onAnimationEnd(Animation arg0) {
		isAnimationRunning = false;
	}

	@Override
	public void onAnimationRepeat(Animation animation) {

	}

	@Override
	public void onAnimationStart(Animation animation) {
		isAnimationRunning = true;
	}

	private boolean isGifImage() {
		String path = mCurrentItem.data;
		FileInputStream fis = null;
		if (path != null) {
			try {
				fis = new FileInputStream(new File(path));
				return AnimationThread.isGifStream(fis);
			} catch (FileNotFoundException e) {

			} finally {
				try {
					if (fis != null) {
						fis.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return false;
	}

	private OnClickListener mOnButtonClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			showControlLayout();
			switch (v.getId()) {
			case R.id.image_rotate_left_button:
				if (isSlideShowing) {
					toggleSlideShow(false);
				}
				rotateLeft();
				break;
			case R.id.image_rotate_right_button:
				if (isSlideShowing) {
					toggleSlideShow(false);
				}
				rotateRight();
				break;
			case R.id.image_info_button:
				showImageDetailsDialog(mCurrentItem);
				break;
			case R.id.image_slide_button:
				toggleSlideShow(!isSlideShowing);
				break;
			case R.id.image_title_text:
				finish();
				break;
			default:
				break;
			}
		}
	};

	private void showImageDetailsDialog(LocalImage image) {
		ScrollView sv = (ScrollView) LayoutInflater.from(this).inflate(R.layout.media_details, null);
		TextView tv = (TextView) sv.findViewById(R.id.mediaDetailsText);
		tv.setText(image.getDetails());
		new AlertDialog.Builder(this, R.style.HoloLargeDialog).setTitle(R.string.media_details).setView(sv)
				.setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {

					}
				}).show();
	}

	private void showControlLayout() {
		if (mImageOperationControl != null) {
			mImageOperationControl.show();
		}
	}

	private void hideControlLayout() {
		if (mImageOperationControl != null) {
			mImageOperationControl.hide();
		}
	}

	private void toggleControlLayout() {
		if (mImageOperationControl != null) {
			mImageOperationControl.toggleOperationLayout();
		}
	}

	private boolean isInTouchMode() {
		if (mImageOperationControl != null) {
			return mImageOperationControl.isInTouchMode();
		}
		return true;
	}

	private void initBroadcastReceiver() {
		IntentFilter filter = new IntentFilter();
		filter.addAction(Intent.ACTION_MEDIA_EJECT);
		filter.addDataScheme("file");
		registerReceiver(mBroadcastReceiver, filter);
	}

	private void releaseBroadcastReceiver() {
		unregisterReceiver(mBroadcastReceiver);
	}

	private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			Log.d(TAG, "onReceive:" + intent.getAction());
			if (intent.getAction().equals(Intent.ACTION_MEDIA_EJECT)) {
				String volume = intent.getData().getPath();
				Log.d(TAG, "volume path:" + volume);
				if (mCurrentItem != null && mCurrentItem.data.startsWith(volume)) {
					if (isSlideShowing) {
						toggleSlideShow(false);
					}
					finish();
				}
			}
		}
	};
}