package com.actions.owlplayer.data;

public interface ItemList {
	public int size();

	public MediaItem get(int position);
}
