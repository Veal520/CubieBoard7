package com.actions.mediacenter.data;

import android.view.View;

public class ActionMode {
	public static final int MODE_SET_CLICK = 0;
	public static final int MODE_SET_SELECT = 1;
	public static final int MODE_ITEM_CLICK = 2;
	public static final int MODE_ITEM_SELECT = 3;

	private int mMode = MODE_SET_CLICK;
	private View mDeleteButton = null;

	public void setMode(int mode) {
		mMode = mode;
		if (isSetClick() || isItemClick()) {
			mDeleteButton.setVisibility(View.INVISIBLE);
		} else {
			mDeleteButton.setVisibility(View.VISIBLE);
		}
	}

	public boolean isSetClick() {
		return mMode == MODE_SET_CLICK;
	}

	public boolean isItemClick() {
		return mMode == MODE_ITEM_CLICK;
	}

	public boolean isSetSelect() {
		return mMode == MODE_SET_SELECT;
	}

	public boolean isItemSelect() {
		return mMode == MODE_ITEM_SELECT;
	}
	
	public boolean isSelectMode() {
		return isItemSelect() || isSetSelect();
	}

	public void setDeleteButton(View button) {
		mDeleteButton = button;
	}
	
	public boolean isItemMode(){
		return isItemClick() || isItemSelect();
	}
}
