package com.actions.owlplayer.sub;

import java.util.ArrayList;

import android.content.Context;

public interface SubtitleManager {

	public boolean initSubTracks(String filename);

	public void releaseSub();

	public int selectSubTrack(int which);

	public String updateSubtitle(int mPeriodPosition);

	public ArrayList<String> getAlertDialogShowList(Context mContext);

}
