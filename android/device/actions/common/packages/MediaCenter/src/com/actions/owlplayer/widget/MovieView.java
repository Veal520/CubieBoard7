package com.actions.owlplayer.widget;

import java.io.IOException;

import android.content.Context;
import android.content.res.TypedArray;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnTimedTextListener;
import android.net.Uri;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import com.actions.mediacenter.R;
import com.actions.owlplayer.app.Config;
import com.actions.owlplayer.util.Utils;

public class MovieView extends SurfaceView implements MovieViewInterface {
	private final String TAG = "MovieView";

	// settable by the client
	private Uri mUri;
	private int mDuration;

	private Context mContext;
	// all possible internal states
	private static final int STATE_ERROR = -1;
	private static final int STATE_IDLE = 0;
	private static final int STATE_PREPARING = 1;
	private static final int STATE_PREPARED = 2;
	private static final int STATE_PLAYING = 3;
	private static final int STATE_PAUSED = 4;
	private static final int STATE_PLAYBACK_COMPLETED = 5;

	// mCurrentState is a VideoView object's current state.
	// mTargetState is the state that a method caller intends to reach.
	// For instance, regardless the VideoView object's current state,
	// calling pause() intends to bring the object to a target state
	// of STATE_PAUSED.
	private int mCurrentState = STATE_IDLE;
	private int mTargetState = STATE_IDLE;

	// All the stuff we need for playing and showing a video
	private SurfaceHolder mSurfaceHolder = null;
	private HwMediaPlayer mMediaPlayer = null;
	private int mVideoWidth;
	private int mVideoHeight;
	private LayoutController mLayoutController;
	private OnCompletionListener mOnCompletionListener;
	private OnErrorListener mOnErrorListener;
	private OnPreparedListener mOnPreparedListener;

	private int mCurrentBufferPercentage;
	private int mSeekWhenPrepared; // recording the seek position while
									// preparing

	// indicate arbitrary scale (used in window mode), default false
	private boolean mArbitraryScale;
	// indicate the scale mode
	private int mScaleMode;
	private Object mPlayerLock = new Object();

	public MovieView(Context context) {
		super(context);
		initVideoView(context);
	}

	public MovieView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(attrs);
		initVideoView(context);
	}

	public MovieView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(attrs);
		initVideoView(context);
	}

	private void init(AttributeSet attrs) {
		TypedArray t = getContext().obtainStyledAttributes(attrs, R.styleable.MovieView);

		for (int i = 0; i < t.getIndexCount(); i++) {
			int attr = t.getIndex(i);
			switch (attr) {
			case R.styleable.MovieView_arbitrary_scale:
				mArbitraryScale = t.getBoolean(R.styleable.MovieView_arbitrary_scale, false);
				break;
			}
		}

		t.recycle();
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int width = getDefaultSize(mVideoWidth, widthMeasureSpec);
		int height = getDefaultSize(mVideoHeight, heightMeasureSpec);
		// Log.i(TAG, "default:" + width + 'x' + height);
		if (!mArbitraryScale && mScaleMode != Config.ViewMode.EXTEND) {
			if (mVideoWidth > 0 && mVideoHeight > 0) {
				if (mScaleMode == Config.ViewMode.ORIGINAL && width > mVideoWidth && height > mVideoHeight) {
					// original
					if (width > mVideoWidth) {
						width = mVideoWidth;
					}

					if (height > mVideoHeight) {
						height = mVideoHeight;
					}
				} else {
					// proportional scale
					if (mVideoWidth * height > width * mVideoHeight) {
						// Log.i(TAG, "image too tall, correcting");
						height = width * mVideoHeight / mVideoWidth;
					} else if (mVideoWidth * height < width * mVideoHeight) {
						// Log.i(TAG, "image too wide, correcting");
						width = height * mVideoWidth / mVideoHeight;
					}
				}
			}
		} else {
			getHolder().setFixedSize(width, height);
		}

		// Log.i(TAG, "onMeasure: " + width + 'x' + height);
		setMeasuredDimension(width, height);
	}

	@Override
	public void onInitializeAccessibilityEvent(AccessibilityEvent event) {
		super.onInitializeAccessibilityEvent(event);
		event.setClassName(MovieView.class.getName());
	}

	@Override
	public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo info) {
		super.onInitializeAccessibilityNodeInfo(info);
		info.setClassName(MovieView.class.getName());
	}

	public int resolveAdjustedSize(int desiredSize, int measureSpec) {
		int result = desiredSize;
		int specMode = MeasureSpec.getMode(measureSpec);
		int specSize = MeasureSpec.getSize(measureSpec);

		switch (specMode) {
		case MeasureSpec.UNSPECIFIED:
			/*
			 * Parent says we can be as big as we want. Just don't be larger than max size imposed on ourselves.
			 */
			result = desiredSize;
			break;

		case MeasureSpec.AT_MOST:
			/*
			 * Parent says we can be as big as we want, up to specSize. Don't be larger than specSize, and don't be larger than the max size imposed on
			 * ourselves.
			 */
			result = Math.min(desiredSize, specSize);
			break;

		case MeasureSpec.EXACTLY:
			// No choice. Do what we are told.
			result = specSize;
			break;
		}

		Log.i(TAG, "resolveAdjustedSize: " + desiredSize + ' ' + measureSpec);
		return result;
	}

	private void initVideoView(Context context) {
		mContext = context;
		mVideoWidth = 0;
		mVideoHeight = 0;
		getHolder().addCallback(mSHCallback);
		setFocusable(true);
		setFocusableInTouchMode(true);
		requestFocus();
		mCurrentState = STATE_IDLE;
		mTargetState = STATE_IDLE;

		mScaleMode = Utils.initViewMode(context);
	}

	public void setVideoPath(String path) {
		setVideoURI(Uri.parse(path));
	}

	public void setVideoURI(Uri uri) {
		mUri = uri;
		mSeekWhenPrepared = 0;
		openVideo();
		requestLayout();
		invalidate();
	}

	public void stopPlayback() {
		Log.v(TAG, "stopPlayback");
		release(true);
	}

	private void openVideo() {
		if (mUri == null || mSurfaceHolder == null) {
			// not ready for playback just yet, will try again later
			return;
		}

		// we shouldn't clear the target state, because somebody might have
		// called start() previously
		release(false);
		try {
			mMediaPlayer = new HwMediaPlayer();

			mMediaPlayer.setOnPreparedListener(mPreparedListener);
			mMediaPlayer.setOnVideoSizeChangedListener(mSizeChangedListener);
			mDuration = -1;
			mMediaPlayer.setOnCompletionListener(mCompletionListener);
			mMediaPlayer.setOnErrorListener(mErrorListener);
			mMediaPlayer.setOnBufferingUpdateListener(mBufferingUpdateListener);
			mCurrentBufferPercentage = 0;

			mMediaPlayer.setDataSource(mContext, mUri);
			mMediaPlayer.setDisplay(mSurfaceHolder);

			// mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
			mMediaPlayer.setScreenOnWhilePlaying(true);
			mMediaPlayer.prepareAsync();
			// we don't set the target state here either, but preserve the
			// target state that was there before.
			mCurrentState = STATE_PREPARING;
		} catch (IOException | NullPointerException ex) {
			Log.w(TAG, "Unable to open content: " + mUri, ex);
			mCurrentState = STATE_ERROR;
			mTargetState = STATE_ERROR;
			mErrorListener.onError(mMediaPlayer, MediaPlayer.MEDIA_ERROR_UNKNOWN, 0);
			return;
		} catch (IllegalArgumentException ex) {
			Log.w(TAG, "Unable to open content: " + mUri, ex);
			mCurrentState = STATE_ERROR;
			mTargetState = STATE_ERROR;
			mErrorListener.onError(mMediaPlayer, MediaPlayer.MEDIA_ERROR_UNKNOWN, 0);
			return;
		}
	}

	public void setLayoutController(LayoutController controller) {
		mLayoutController = controller;
	}

	MediaPlayer.OnVideoSizeChangedListener mSizeChangedListener = new MediaPlayer.OnVideoSizeChangedListener() {
		public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
			// Log.i(TAG, "onVideoSizeChanged: " + width + 'x' + height);
			mVideoWidth = mp.getVideoWidth();
			mVideoHeight = mp.getVideoHeight();
			if (mVideoWidth != width || mVideoHeight != height) {
				Log.w(TAG, "something goes wrong with the video size");
			}
			getHolder().setFixedSize(mVideoWidth, mVideoHeight);
			/*
			 * make sure the onMeasure will be called while video size changed (file change) not only depend on the controller show/hide()
			 */
			requestLayout();
		}
	};

	MediaPlayer.OnPreparedListener mPreparedListener = new MediaPlayer.OnPreparedListener() {
		public void onPrepared(MediaPlayer mp) {
			synchronized (mPlayerLock) {
				if (mMediaPlayer == null) {
					return;
				}

				mCurrentState = STATE_PREPARED;

				if (mOnPreparedListener != null) {
					mOnPreparedListener.onPrepared();
				}

				mVideoWidth = mp.getVideoWidth();
				mVideoHeight = mp.getVideoHeight();

				int seekToPosition = mSeekWhenPrepared;
				if (seekToPosition != 0) {
					seekTo(seekToPosition);
				}

				if (mTargetState == STATE_PLAYING) {
					start();
					if (mLayoutController != null) {
						mLayoutController.show();
					}
				}
			}
		}
	};

	private MediaPlayer.OnCompletionListener mCompletionListener = new MediaPlayer.OnCompletionListener() {
		public void onCompletion(MediaPlayer mp) {
			synchronized (mPlayerLock) {
				if (mMediaPlayer == null) {
					return;
				}

				mCurrentState = STATE_PLAYBACK_COMPLETED;
				mTargetState = STATE_PLAYBACK_COMPLETED;
				if (mLayoutController != null) {
					mLayoutController.show();
				}

				if (mOnCompletionListener != null) {
					mOnCompletionListener.onCompletion();
				}
			}
		}
	};

	private MediaPlayer.OnErrorListener mErrorListener = new MediaPlayer.OnErrorListener() {
		public boolean onError(MediaPlayer mp, int framework_err, int impl_err) {
			synchronized (mPlayerLock) {
				if (mMediaPlayer == null) {
					Log.w(TAG, "null while error");
					return true;
				}

				Log.e(TAG, "Error: " + framework_err + "," + impl_err);
				mCurrentState = STATE_ERROR;
				mTargetState = STATE_ERROR;
				if (mLayoutController != null) {
//					mLayoutController.hide();
				}

				/* If an error handler has been supplied, use it and finish. */
				if (mOnErrorListener != null) {
					if (mOnErrorListener.onError(framework_err, impl_err)) {
						return true;
					}
				}
			}

			return true;
		}
	};

	private MediaPlayer.OnBufferingUpdateListener mBufferingUpdateListener = new MediaPlayer.OnBufferingUpdateListener() {
		public void onBufferingUpdate(MediaPlayer mp, int percent) {
			mCurrentBufferPercentage = percent;
		}
	};

	public void setOnPreparedListener(OnPreparedListener l) {
		mOnPreparedListener = l;
	}

	/**
	 * Register a callback to be invoked when the end of a media file has been reached during playback.
	 * 
	 * @param l
	 *            The callback that will be run
	 */
	public void setOnCompletionListener(OnCompletionListener l) {
		mOnCompletionListener = l;
	}

	/**
	 * Register a callback to be invoked when an error occurs during playback or setup. If no listener is specified, or if the listener returned false,
	 * VideoView will inform the user of any errors.
	 * 
	 * @param l
	 *            The callback that will be run
	 */
	public void setOnErrorListener(OnErrorListener l) {
		mOnErrorListener = l;
	}

	SurfaceHolder.Callback mSHCallback = new SurfaceHolder.Callback() {
		public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
		}

		public void surfaceCreated(SurfaceHolder holder) {
			Log.d(TAG, "surfaceCreated");
			mSurfaceHolder = holder;
			openVideo();
		}

		public void surfaceDestroyed(SurfaceHolder holder) {
			Log.d(TAG, "surfaceDestroyed");
			// after we return from this we can't use the surface any more
			mSurfaceHolder = null;
			// fix bug while next surface created before mUri is set
			mUri = null;
			if (mLayoutController != null) {
//				mLayoutController.hide();
			}
			release(true);
		}
	};

	/*
	 * release the media player in any state
	 */
	private void release(boolean cleartargetstate) {
		synchronized (mPlayerLock) {
			if (mMediaPlayer != null) {
				mMediaPlayer.reset();
				mMediaPlayer.release();
				mMediaPlayer = null;
				mCurrentState = STATE_IDLE;
				if (cleartargetstate) {
					mTargetState = STATE_IDLE;
				}
			}
		}
	}

	private GestureDetector mSimpleGestureDetector = new GestureDetector(mContext, new GestureDetector.SimpleOnGestureListener() {
		@Override
		public boolean onDoubleTap(MotionEvent e) {
			//forbid change view mode
/*			mScaleMode = (mScaleMode == Config.ViewMode.ORIGINAL) ? Config.ViewMode.FIT : ((mScaleMode == Config.ViewMode.FIT) ? Config.ViewMode.EXTEND
					: Config.ViewMode.ORIGINAL);

			requestLayout();*/
			return true;
		}

		@Override
		public boolean onSingleTapConfirmed(MotionEvent e) {
			if (isInPlaybackState() && mLayoutController != null) {
				toggleMediaControlsVisiblity();
			}

			return true;
		}
	});

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		mSimpleGestureDetector.onTouchEvent(event);
		return true;
	}

	private void toggleMediaControlsVisiblity() {
		if (mLayoutController.isShowing()) {
			mLayoutController.hide();
		} else {
			mLayoutController.show();
		}
	}

	private boolean isInPlaybackState() {
		return (mMediaPlayer != null && mCurrentState != STATE_ERROR && mCurrentState != STATE_IDLE && mCurrentState != STATE_PREPARING);
	}

	public void start() {
		if (isInPlaybackState()) {
			if (mCurrentState == STATE_PLAYBACK_COMPLETED && mUri.getScheme().contains("http")) {
				openVideo();
				Log.i(TAG, "start() called. need openVideo()");
			} else {
				mMediaPlayer.start();
				mCurrentState = STATE_PLAYING;
			}
		}
		mTargetState = STATE_PLAYING;
	}

	public void pause() {
		if (isInPlaybackState()) {
			if (mMediaPlayer.isPlaying()) {
				mMediaPlayer.pause();

				mCurrentState = STATE_PAUSED;
			}
		}
		mTargetState = STATE_PAUSED;
	}

	public void suspend() {
		release(false);
	}

	public void resume() {
		openVideo();
	}

	// cache duration as mDuration for faster access
	public int getDuration() {
		if (isInPlaybackState()) {
			if (mDuration <= 0) {
				mDuration = (int) mMediaPlayer.getDuration();
			}
		} else {
			mDuration = -1;
		}

		return mDuration;
	}

	public int getCurrentPosition() {
		if (isInPlaybackState()) {
			return (int) mMediaPlayer.getCurrentPosition();
		}
		return 0;
	}

	public void seekTo(int msec) {
		if (isInPlaybackState()) {
			mMediaPlayer.seekTo(msec);
			mSeekWhenPrepared = 0;
		} else {
			mSeekWhenPrepared = msec;
		}
	}

	public boolean isPlaying() {
		return isInPlaybackState() && mMediaPlayer.isPlaying();
	}

	public int getBufferPercentage() {
		if (mMediaPlayer != null) {
			return mCurrentBufferPercentage;
		}
		return 0;
	}

	public void setVolume(float left, float right) {
		if (mMediaPlayer != null) {
			mMediaPlayer.setVolume(left, right);
		}
	}

	public void selectTrack(int index) {
		mMediaPlayer.selectTrack(index);
	}

	public CustomTrackInfo[] getTrackMetadata() {
		try {
			return mMediaPlayer.getTrackMetadata();
		} catch (Exception e) {
			return null;
		}
	}

	public boolean onExtraTouchEvent(MotionEvent event) {
		return this.onTouchEvent(event);
	}

	public void addTimedTextSource(String path, String mimeType) {
		try {
			mMediaPlayer.addTimedTextSource(path, mimeType);
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void setOnTimedTextListener(OnTimedTextListener listener) {
		mMediaPlayer.setOnTimedTextListener(listener);
	}

	@Override
	public void deselectTrack(int index) {
		mMediaPlayer.deselectTrack(index);
	}

	@Override
	public void addActTimedTextSource(String path, String mimeType) {
		try {
			mMediaPlayer.addActTimedTextSource(path, mimeType);
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
