package com.actions.owlplayer.app;

import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.GridLayoutManager.LayoutParams;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.actions.mediacenter.R;
import com.actions.mediacenter.widget.BaseRecyclerViewAdapter;
import com.actions.owlplayer.app.BitmapLoader.ImageCallback;
import com.actions.owlplayer.data.LocalVideo;
import com.actions.owlplayer.data.LocalVideoSet;
import com.actions.owlplayer.data.VideoSetManager;

public class VideoSetAdapter extends BaseRecyclerViewAdapter<VideoSetAdapter.ViewHolder> {
	private static final String TAG = VideoSetAdapter.class.getSimpleName();
	private VideoSetManager mVideoSetManager;
	private BitmapLoader mBitmapLoader;
	private OnVideoSetClickListener mOnVideoSetClickListener;
	private OnVideoSetLongClickListener mOnVideoSetLongClickListener;
	private OWLApplication mApplication;

	public VideoSetAdapter(OWLApplication application, VideoSetManager manager) {
		mApplication = application;
		mVideoSetManager = manager;
		mBitmapLoader = BitmapLoader.getInstance(application);
	}

	@Override
	public int getItemCount() {
		return mVideoSetManager.size();
	}

	@Override
	public void onBindViewHolder(ViewHolder viewHolder, int position) {
		super.onBindViewHolder(viewHolder, position);
		viewHolder.itemView.setTag(position);
		viewHolder.itemView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (mOnVideoSetClickListener != null) {
					mOnVideoSetClickListener.onItemClick(v, (int) v.getTag());
				}
			}
		});
		viewHolder.itemView.setOnLongClickListener(new OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				if (mOnVideoSetLongClickListener != null) {
					return mOnVideoSetLongClickListener.onItemLongClick(v, (int) v.getTag());
				}
				return false;
			}
		});
		LocalVideoSet videoSet = (LocalVideoSet) mVideoSetManager.get(position);
		viewHolder.videoSetName.setText(videoSet.getName());
		viewHolder.videoSetItemCount.setText(Integer.toString(videoSet.size()));
		LocalVideo item = (LocalVideo) mVideoSetManager.get(position).getItem(0);
		viewHolder.videoSetImage.setTag(item.videoId);
		Bitmap bitmap = mBitmapLoader.loadBitmap(item, viewHolder.videoSetImage, new ImageCallback() {

			@Override
			public void imageLoad(int viewTag, ImageView imageView, Bitmap bitmap) {
				if (imageView.getTag().equals(viewTag)) {
					imageView.setImageBitmap(bitmap);
				}
			}
		});
		if (bitmap != null) {
			viewHolder.videoSetImage.setImageBitmap(bitmap);
		} else {
			viewHolder.videoSetImage.setImageBitmap(null);
		}
		if (videoSet.isSelect()) {
			viewHolder.selectImage.setVisibility(View.VISIBLE);
		} else {
			viewHolder.selectImage.setVisibility(View.INVISIBLE);
		}
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
		View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.video_set_item, viewGroup, false);
		initLayout(viewGroup, view);
		return new ViewHolder(view);
	}

	public class ViewHolder extends RecyclerView.ViewHolder {
		ImageView videoSetImage;
		TextView videoSetName;
		TextView videoSetItemCount;
		View selectImage;

		public ViewHolder(View rootView) {
			super(rootView);
			videoSetImage = (ImageView) rootView.findViewById(R.id.video_set_image);
			videoSetName = (TextView) rootView.findViewById(R.id.video_set_title);
			videoSetItemCount = (TextView) rootView.findViewById(R.id.video_set_count);
			selectImage = rootView.findViewById(R.id.media_select_image);
		}

	}

	public void setOnVideoSetClickListener(OnVideoSetClickListener listener) {
		mOnVideoSetClickListener = listener;
	}

	public void setOnVideoSetLongClickListener(OnVideoSetLongClickListener listener) {
		mOnVideoSetLongClickListener = listener;
	}

	public interface OnVideoSetClickListener {
		public void onItemClick(View view, int position);
	}

	public interface OnVideoSetLongClickListener {
		public boolean onItemLongClick(View view, int position);
	}
	
	private void initLayout(ViewGroup viewGroup, View view) {
		Config.initThumbLayoutParams(viewGroup, view);
		LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
		layoutParams.setMargins(Config.thumbnailMarginLeft, Config.thumbnailMarginTop, Config.thumbnailMarginRight, Config.thumbnailMarginButtom);
		layoutParams.height = Config.thumbnailHeight;
		layoutParams.width = Config.thumbnailWidth;
		view.setLayoutParams(layoutParams);
	}
}
