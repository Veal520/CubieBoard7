package com.actions.owlplayer.sub;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnTimedTextListener;
import android.media.TimedText;
import android.util.Log;
import android.util.SparseArray;

import com.actions.mediacenter.R;
import com.actions.owlplayer.widget.CustomTrackInfo;
import com.actions.owlplayer.widget.MovieViewInterface;

public class HwSubtitleManager implements SubtitleManager {
	private String TAG = "SubtitileManager";
	private String MEDIA_MIMETYPE_TEXT_SUBRIP = "application/x-subrip";
	private String MEDIA_MIMETYPE_TEXT_ACT_ASS = "text/ass";
	private String MEDIA_MIMETYPE_TEXT_ACT_SSA = "text/ssa";
	private String MEDIA_MIMETYPE_TEXT_ACT_SMI = "text/smi";

	private MovieViewInterface mPlayer;
	private int SUB_TRACK_OFFSET = 100;
	private String mFilename;
	private int mAudioTrackCount = 0;
	private int mEmbeddedSubTrackCount = 0;
	private SparseArray<CustomTrackInfo> mTrackInfo;
	private ArrayList<Integer> mEnbeddedSubTrackIndexList;
	private int mSubTrackSelected = -1;
	private String subContent;

	public HwSubtitleManager(MovieViewInterface mPlayer, SparseArray<CustomTrackInfo> mTrackInfo, int mSubTrackSelected) {
		this.mPlayer = mPlayer;
		this.mTrackInfo = mTrackInfo;
		this.mSubTrackSelected = mSubTrackSelected;
		mEnbeddedSubTrackIndexList = new ArrayList<Integer>();
	}

	@Override
	public boolean initSubTracks(String filename) {
		mFilename = filename;
		int track = 0;
		int size = mTrackInfo.size();
		mAudioTrackCount = size;
		releaseSub();
		mEnbeddedSubTrackIndexList.clear();

		CustomTrackInfo[] trackInfo = mPlayer.getTrackMetadata();
		if (trackInfo != null) {
			track = SUB_TRACK_OFFSET;
			for (int i = 0; i < trackInfo.length; i++) {
				CustomTrackInfo t = trackInfo[i];
				if (t.getTrackType() == CustomTrackInfo.MEDIA_TRACK_TYPE_TIMEDTEXT) {
					mEnbeddedSubTrackIndexList.add(i);
					mTrackInfo.append(track, t);
				}
				track++;
			}
		}

		mEmbeddedSubTrackCount = trackInfo.length - mAudioTrackCount - 1;
		Log.d(TAG, "mEmbeddedSubTrackCount:" + mEmbeddedSubTrackCount);
		Log.d(TAG, "mSubTrackSelected:" + mSubTrackSelected);
		if (mSubTrackSelected != -1)
			selectSubTrack(mSubTrackSelected);

		return (trackInfo != null && mTrackInfo.size() > size) || getSubPathWithSamePrefix(mFilename).size() > 0;
	}

	@Override
	public void releaseSub() {

	}

	@Override
	public int selectSubTrack(int which) {
		if (mPlayer == null) {
			return -1;
		}
		if (which < mEnbeddedSubTrackIndexList.size()) {
			mPlayer.setOnTimedTextListener(mOnTimedTextListener);
			int embedbedSubTrackIndex = mEnbeddedSubTrackIndexList.get(which);
			try {
				mPlayer.selectTrack(embedbedSubTrackIndex);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			String subTitleName = getSubPathWithSamePrefix(mFilename).get(which - mEnbeddedSubTrackIndexList.size());

			String subTitlePath = mFilename.substring(0, mFilename.lastIndexOf('/') + 1) + subTitleName;
			Log.d(TAG, "subTitlePath:" + subTitlePath);
			mPlayer.setOnTimedTextListener(mOnTimedTextListener);
			mPlayer.addActTimedTextSource(subTitlePath, getSubFormat(subTitlePath));
			Log.d(TAG, "add addTimedTextSource");

			int selection = mPlayer.getTrackMetadata().length - 1;
			Log.d(TAG, "selection:" + selection);

			try {
				mPlayer.selectTrack(selection);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// clear sub text
			subContent = "";
		}
		mSubTrackSelected = which;
		return mSubTrackSelected;
	}

	@Override
	public String updateSubtitle(int mPeriodPosition) {
		return subContent;
	}

	@Override
	public ArrayList<String> getAlertDialogShowList(Context mContext) {
		ArrayList<String> list = new ArrayList<String>();
		int track = 0;
		for (int i = 0; i < mTrackInfo.size(); i++) {
			CustomTrackInfo t = mTrackInfo.valueAt(i);
			if (t.getTrackType() == CustomTrackInfo.MEDIA_TRACK_TYPE_TIMEDTEXT) {
				list.add(mContext.getString(R.string.message_sub_track, track, t.getLanguage()));
				track++;
			}
		}
		List<String> plugInSub = getSubPathWithSamePrefix(mFilename);
		for (int i = 0; i < plugInSub.size(); i++) {
			list.add(mContext.getString(R.string.message_sub_track, track, plugInSub.get(i)));
			track++;
		}
		return list;
	}

	private List<String> getSubPathWithSamePrefix(String videoFilePath) {
		if (videoFilePath == null || !(new File(videoFilePath)).exists()) {
			return new ArrayList<String>();
		}
		final String filePath = videoFilePath;
		File fileDir = new File(filePath.substring(0, filePath.lastIndexOf('/')));
		String[] files = fileDir.list(new FilenameFilter() {

			@Override
			public boolean accept(File dir, String filename) {
				if (!filePath.equals(filename) && filename.startsWith(filePath.substring(filePath.lastIndexOf('/') + 1, filePath.lastIndexOf('.')))) {
					String suffix = filename.substring(filename.lastIndexOf('.') + 1).toLowerCase();
					if (suffix.equals("srt") || suffix.equals("ssa") || suffix.equals("ass") || suffix.equals("smi") || suffix.equals("sami"))
						return true;
				}

				return false;
			}
		});
		return Arrays.asList(files);
	}

	private OnTimedTextListener mOnTimedTextListener = new OnTimedTextListener() {

		@Override
		public void onTimedText(MediaPlayer mp, TimedText text) {

			if (text != null) {
				subContent = text.getText();
				Log.d(TAG, "subcontext:" + text.getText());
			} else {
				subContent = "";
				Log.d(TAG, "subcontext:");
			}
		}
	};

	private String getSubFormat(String fileName) {
		String formatString = fileName.substring(fileName.lastIndexOf('.') + 1).toLowerCase();
		if (formatString.equals("srt")) {
			return MEDIA_MIMETYPE_TEXT_SUBRIP;
		} else if (formatString.equals("ssa")) {
			return MEDIA_MIMETYPE_TEXT_ACT_SSA;
		} else if (formatString.equals("ass")) {
			return MEDIA_MIMETYPE_TEXT_ACT_ASS;
		} else if (formatString.equals("smi") || formatString.equals("sami")) {
			return MEDIA_MIMETYPE_TEXT_ACT_SMI;
		}
		return null;
	}

}
