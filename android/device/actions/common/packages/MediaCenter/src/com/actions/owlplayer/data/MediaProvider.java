package com.actions.owlplayer.data;

import java.util.ArrayList;

import android.content.ContentProvider;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.OperationApplicationException;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

public class MediaProvider extends ContentProvider {
	private final static String TAG = "MediaProvider";

	public final static String AUTHORITY = "com.actions.mediacenter.data.videoplayer";
	private MediaDatabaseOpenHelper mDBOpenHelper;
	private SQLiteDatabase mSQLiteDatabase;
	private static final int VIDEO = 1;
	private static final UriMatcher mURLMatcher = new UriMatcher(UriMatcher.NO_MATCH);

	public static final Uri VIDEO_CONTENT_URI = Uri.parse("content://com.actions.mediacenter.data.videoplayer/video");

	static {
		mURLMatcher.addURI(AUTHORITY, "video", VIDEO);
	}

	public MediaProvider() {

	}

	@Override
	public boolean onCreate() {
		mDBOpenHelper = new MediaDatabaseOpenHelper(getContext());
		return true;
	}

	@Override
	public String getType(Uri url) {
		int match = mURLMatcher.match(url);
		switch (match) {
		case VIDEO:
			return AUTHORITY+"/video";
		default:
			throw new IllegalArgumentException("Unknown URL");
		}
	}

	@Override
	public Uri insert(Uri url, ContentValues contentValues) {
		if (mURLMatcher.match(url) != VIDEO || contentValues == null) {
			throw new IllegalArgumentException("Cannot insert into URL: " + url);
		}

		if (mSQLiteDatabase == null || !mSQLiteDatabase.isOpen() || mSQLiteDatabase.isReadOnly()) {
			mSQLiteDatabase = mDBOpenHelper.getWritableDatabase();
		}

		long rowId = mSQLiteDatabase.insert(MediaDatabaseOpenHelper.DB_TAB_VIDEO, null, contentValues);
		if (rowId < 0) {
			throw new SQLException("Failed to insert row into " + url);
		}

		Uri newUrl = ContentUris.withAppendedId(url, rowId);
		getContext().getContentResolver().notifyChange(newUrl, null);

		return newUrl;
	}

	@Override
	public int delete(Uri url, String where, String[] whereArgs) {
		int count;
		if (mSQLiteDatabase == null || !mSQLiteDatabase.isOpen() || mSQLiteDatabase.isReadOnly()) {
			mSQLiteDatabase = mDBOpenHelper.getWritableDatabase();
		}
		switch (mURLMatcher.match(url)) {
		case VIDEO:
			count = mSQLiteDatabase.delete(MediaDatabaseOpenHelper.DB_TAB_VIDEO, where, whereArgs);
			break;
		default:
			throw new IllegalArgumentException("Cannot delete from URL: " + url);
		}

		getContext().getContentResolver().notifyChange(url, null);

		return count;
	}

	@Override
	public int update(Uri url, ContentValues values, String where, String[] whereArgs) {
		int count;
		int match = mURLMatcher.match(url);
		if (mSQLiteDatabase == null || !mSQLiteDatabase.isOpen() || mSQLiteDatabase.isReadOnly()) {
			mSQLiteDatabase = mDBOpenHelper.getWritableDatabase();
		}
		switch (match) {
		case VIDEO:
			count = mSQLiteDatabase.update(MediaDatabaseOpenHelper.DB_TAB_VIDEO, values, where, whereArgs);
			break;
		default:
			throw new UnsupportedOperationException("Cannot update URL: " + url);
		}

		getContext().getContentResolver().notifyChange(url, null);

		return count;
	}

	@Override
	public Cursor query(Uri url, String[] projection, String where, String[] whereArgs, String sortOrder) {
		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
		int match = mURLMatcher.match(url);
		switch (match) {
		case VIDEO:
			qb.setTables(MediaDatabaseOpenHelper.DB_TAB_VIDEO);
			break;
		default:
			throw new IllegalArgumentException("Unknown URL " + url);
		}

		if (mSQLiteDatabase == null || !mSQLiteDatabase.isOpen()) {
			mSQLiteDatabase = mDBOpenHelper.getWritableDatabase();
		}

		Cursor cursor = qb.query(mSQLiteDatabase, projection, where, whereArgs, null, null, sortOrder);
		if (cursor != null) {
			cursor.setNotificationUri(getContext().getContentResolver(), url);
		}
		return cursor;
	}

	@Override
	public ContentProviderResult[] applyBatch(ArrayList<ContentProviderOperation> operations) throws OperationApplicationException {
		mSQLiteDatabase.beginTransaction();
		try {
			ContentProviderResult[] results = super.applyBatch(operations);
			mSQLiteDatabase.setTransactionSuccessful();
			return results;
		} finally {
			mSQLiteDatabase.endTransaction();
		}
	}

	@Override
	public void shutdown() {
		if (mSQLiteDatabase != null && mSQLiteDatabase.isOpen()) {
			mSQLiteDatabase.close();
		}
	}
}