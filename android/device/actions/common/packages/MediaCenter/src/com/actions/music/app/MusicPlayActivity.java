package com.actions.music.app;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.AudioManager.OnAudioFocusChangeListener;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.actions.mediacenter.R;
import com.actions.mediacenter.app.BaseActivity;
import com.actions.mediacenter.utils.CommonUtil;
import com.actions.mediacenter.widget.ImageButtonWithState;
import com.actions.music.data.LocalMusic;
import com.actions.music.data.MusicItemCache;
import com.actions.music.data.MusicLooper;
import com.actions.music.data.PlayList;
import com.actions.music.data.PlayList.ListItem;
import com.actions.music.data.PlayList.OnPlayListUpdateListener;
import com.actions.music.data.PlayRecord;
import com.actions.music.lyric.MusicLyricView;
import com.actions.music.widget.BaseVisualizerView;
import com.actions.music.widget.ErrorMusicCounter;

public class MusicPlayActivity extends BaseActivity implements OnClickListener {
	private static final String TAG = MusicPlayActivity.class.getSimpleName();

	private static final int MESSAGE_AUTOUPDATE_TIME = 0;

	private String[] mRepeatTexts;

	private ListView mListView;
	private SeekBar mSeekBar;
	private ImageButton mPrevButton;
	private ImageButton mNextButton;
	private ImageButtonWithState mToggleButton;
	private ImageButton mRepeatButton;
	private TextView mMusicTitle;
	private TextView mMusicCurrentTime;
	private TextView mMusicDuration;
	private BaseVisualizerView mVisualizerView;
	private MusicLyricView mLyricView;
	private ViewPager mViewPager;
	private TextView mBackButton;
	private TextView mMusicIndexView;
	private TextView mMusicCountView;
	private TextView mMusicRepeatText;
	private ImageButton mButtonAdjustVolume;

	private MusicItemCache mMusicItemCache;
	private MusicItemAdapter mMusicItemAdapter;
	private Context mContext;
	private AudioManager mAudioManager;
	private MediaPlayer mMediaPlayer;
	private boolean isSeekBarTracking = false;
	private int mCurrentTime;
	private MusicLooper mMusicLooper;
	private PlayList mPlayList;
	private PlayRecord mPlayRecord;
	private MusicPagerAdapter mPagerAdapter;
	private List<View> mPagerViewList = new ArrayList<View>();
	private boolean mEnableUpdate = true;
	private boolean mStopPlayInBackground = false;
	private ErrorMusicCounter mErrorMusicCounter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "onCreate");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.music_play_view);

		mContext = this;
		mMusicItemCache = MusicItemCache.getInstance(this);
		mMusicItemCache.update();
		mRepeatTexts = getResources().getStringArray(R.array.array_repeat_text);

		mErrorMusicCounter = new ErrorMusicCounter();
		mMusicLooper = MusicLooper.getInstance(mContext);
		mPlayRecord = PlayRecord.getInstance(mContext);
		mPlayList = new PlayList(mContext, mPlayRecord);
		mPlayList.setOnPlayListUpdateListener(mOnPlayListUpdateListener);
		initShowLayout();
		initControlLayout();
		initViewPager();
		initPlayListView();
		initBroadcastReceiver();
		startAutoUpdate();
	}

	private void initBroadcastReceiver() {
		IntentFilter filter = new IntentFilter();
		filter.addAction(Intent.ACTION_SHUTDOWN);
		registerReceiver(mBroadcastReceiver, filter);
	}

	private void initPlayListView() {
		mListView = (ListView) findViewById(R.id.music_playlist);
		mListView.setOnKeyListener(mListViewOnKeyListener);
		mMusicItemAdapter = new MusicItemAdapter(mContext);
		mListView.setOnItemClickListener(mOnItemClickListener);
		mListView.setAdapter(mMusicItemAdapter);
		updatePlayList(mPlayRecord.getCurrentIndex());
	}

	private void initControlLayout() {
		mBackButton = (TextView) findViewById(R.id.back_button);
		mBackButton.setOnClickListener(this);

		mSeekBar = (SeekBar) findViewById(R.id.music_seekbar);
		mSeekBar.setOnSeekBarChangeListener(mOnSeekBarChangeListener);
		mPrevButton = (ImageButton) findViewById(R.id.music_previous);
		mPrevButton.setOnClickListener(this);
		mNextButton = (ImageButton) findViewById(R.id.music_next);
		mNextButton.setOnClickListener(this);
		mToggleButton = (ImageButtonWithState) findViewById(R.id.music_toggle);
		mToggleButton.setState(ImageButtonWithState.STATE_PLAY);
		mToggleButton.setOnClickListener(this);
		mRepeatButton = (ImageButton) findViewById(R.id.music_loop);
		mRepeatButton.setOnClickListener(this);
		mButtonAdjustVolume = (ImageButton) findViewById(R.id.button_adjust_volume);
		mButtonAdjustVolume.setOnClickListener(this);

		updateRepeatButton();
		LocalMusic music = getCurrentMusic();
		if (music != null) {
			mSeekBar.setMax(music.getDuration());
			mSeekBar.post(new Runnable() {

				@Override
				public void run() {
					mSeekBar.setProgress(mCurrentTime);
				}
			});
		}
	}
	
	@Override
	public void onBackPressed() {
		goToHomePage();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		Log.d(TAG, "onKeyDown:" + keyCode);
		if (keyCode == KeyEvent.KEYCODE_DPAD_LEFT || keyCode == KeyEvent.KEYCODE_DPAD_RIGHT) {
			event.startTracking();
			if (mKeyLongPress && event.getRepeatCount() % 2 == 0) {
				switch (keyCode) {
				case KeyEvent.KEYCODE_DPAD_LEFT:
					fastForwardAndBackward(false);
					break;
				case KeyEvent.KEYCODE_DPAD_RIGHT:
					fastForwardAndBackward(true);
					break;
				}
			}
			return true;
		}
		if (keyCode == KeyEvent.KEYCODE_MENU) {
			toggleSpectrumAndLyric();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		Log.d(TAG, "onKeyUp:" + keyCode);
		switch (keyCode) {
		case KeyEvent.KEYCODE_DPAD_LEFT:
			if (!mKeyLongPress) {
				navigate(false, true, true);
				return true;
			}
			break;
		case KeyEvent.KEYCODE_DPAD_RIGHT:
			if (!mKeyLongPress) {
				navigate(true, true, true);
				return true;
			}
			break;
		case KeyEvent.KEYCODE_MEDIA_PREVIOUS:
			navigate(false, true, true);
			break;
		case KeyEvent.KEYCODE_MEDIA_NEXT:
			navigate(true, true, true);
			break;
		}
		mKeyLongPress = false;
		return super.onKeyUp(keyCode, event);
	}

	private void initShowLayout() {
		mMusicTitle = (TextView) findViewById(R.id.music_title);
		mMusicCurrentTime = (TextView) findViewById(R.id.music_current_time);
		mMusicDuration = (TextView) findViewById(R.id.music_duration);
		mMusicIndexView = (TextView) findViewById(R.id.music_index);
		mMusicCountView = (TextView) findViewById(R.id.music_count);
		mMusicRepeatText = (TextView) findViewById(R.id.music_loop_text);

		mCurrentTime = 0;
		LocalMusic music = getCurrentMusic();
		if (music == null) {
			return;
		}
		mCurrentTime = mPlayRecord.getMusicBreakpoint();
		updateTitle(music);
		mMusicCurrentTime.setText(CommonUtil.convertMsToHHMMSS(mCurrentTime));
		mMusicDuration.setText(CommonUtil.convertMsToHHMMSS(music.getDuration()));
		mMusicIndexView.setText(getString(R.string.text_music_playing_index) + Integer.toString(mPlayRecord.getCurrentIndex() + 1));
		mMusicCountView.setText(getString(R.string.text_music_count) + Integer.toString(mMusicItemCache.size()));
	}

	private LocalMusic getCurrentMusic() {
		LocalMusic music = mMusicItemCache.getItem(mPlayRecord.getCurrentIndex());
		if (music == null) {
			mPlayRecord.removeFirst();
			music = mMusicItemCache.getItem(0);
			if (music == null) {
				return null;
			}
			mPlayRecord.add(mPlayList.getItemByPosition(0));
			mPlayRecord.save(0);
			resetCurrentTime();
		}
		return music;
	}

	private void initViewPager() {
		mViewPager = (ViewPager) findViewById(R.id.music_view_pager);
		View view1 = LayoutInflater.from(this).inflate(R.layout.music_visualizer_view, null);
		mVisualizerView = (BaseVisualizerView) view1.findViewById(R.id.music_frequency_view);
		View view2 = LayoutInflater.from(this).inflate(R.layout.music_lyric_view, null);
		mLyricView = (MusicLyricView) view2.findViewById(R.id.music_lyric_view);
		mPagerViewList.add(view1);
		mPagerViewList.add(view2);
		mPagerAdapter = new MusicPagerAdapter(mPagerViewList);
		mViewPager.setAdapter(mPagerAdapter);
	}

	@Override
	protected void onResume() {
		Log.d(TAG, "onResume");
		super.onResume();
		mStopPlayInBackground = false;
		if(mVisualizerView!=null) {
			mVisualizerView.resume();
		}
		stopBackgroundMusic();
		prePlayMusic(false);
	}

	@Override
	protected void onPause() {
		Log.d(TAG, "onPause");
		super.onPause();
		if(mVisualizerView!=null) {
			mVisualizerView.pause();
		}
		if(mMediaPlayer!=null && !mMediaPlayer.isPlaying()) {
			mAudioManager.abandonAudioFocus(mAudioFocusChangeListener);
		}
	}

	@Override
	protected void onDestroy() {
		Log.d(TAG, "onDestroy");
		super.onDestroy();
		savePlayParams();
		mMusicItemCache.release();
		release();
		stopAutoUpdate();
	}

	private void savePlayParams() {
		Log.d(TAG, "savePlayParams");
		if (mMediaPlayer != null) {
			Log.d(TAG, "save:"+mMediaPlayer.getCurrentPosition());
			mPlayRecord.save(mMediaPlayer.getCurrentPosition());
		}
		mMusicLooper.save();
	}

	private void release() {
		releaseMediaPlayer();
		mAudioManager.abandonAudioFocus(mAudioFocusChangeListener);
		unregisterReceiver(mBroadcastReceiver);
	}

	private void stopBackgroundMusic() {
		if (mAudioManager == null) {
			mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		}
		int result = mAudioManager.requestAudioFocus(mAudioFocusChangeListener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
		if (result == AudioManager.AUDIOFOCUS_REQUEST_FAILED) {
			Log.i(TAG, "request Audio focus failed");
		}
	}

	private OnAudioFocusChangeListener mAudioFocusChangeListener = new OnAudioFocusChangeListener() {

		@Override
		public void onAudioFocusChange(int focusChange) {
			Log.d(TAG, "onAudioFocusChange:"+focusChange);
			switch (focusChange) {
			case AudioManager.AUDIOFOCUS_LOSS:
			case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
			case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
				Log.d(TAG, "pause");
				mStopPlayInBackground = true;
				pause();
				savePlayParams();
				updateToggleButton();
				break;
			case AudioManager.AUDIOFOCUS_GAIN:
			case AudioManager.AUDIOFOCUS_GAIN_TRANSIENT:
			case AudioManager.AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK:
				mStopPlayInBackground = false;
			default:
				break;
			}
		}
	};

	private void pause() {
		if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
			mMediaPlayer.pause();
		}
	}

	private void playMusic(boolean resetPosition) {
		playMusic(resetPosition, true);
	}

	private void prePlayMusic(boolean resetPosition) {
		if (mMediaPlayer == null) {
			playMusic(resetPosition, false);
		}
	}

	private void playMusic(boolean resetPosition, boolean startPlay) {
		mEnableUpdate = false;
		if (mMediaPlayer == null) {
			mMediaPlayer = new MediaPlayer();
		}
		try {
			mMediaPlayer.reset();
			mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

			LocalMusic music = mMusicItemCache.getItem(mPlayRecord.getCurrentIndex());
			if (music == null) {
				mPlayRecord.removeFirst();
				music = mMusicItemCache.getItem(0);
				if (music == null) {
					releaseMediaPlayer();
					return;
				}
				mPlayRecord.add(mPlayList.getItemByPosition(0));
				mPlayRecord.save(0);
				resetCurrentTime();
			}

			mMediaPlayer.setDataSource(music.getMediaPath().getPath());
			mMediaPlayer.prepare();
			mMediaPlayer.setOnCompletionListener(mOnCompletionListener);
			mMediaPlayer.setOnErrorListener(mOnErrorListener);

			if (mVisualizerView != null) {
				mVisualizerView.releaseVisualizer();
			}

			mVisualizerView.enableDataProcess(true);
			mVisualizerView.setVisualizer(mMediaPlayer.getAudioSessionId(), true);

			mLyricView.setMediaPlayer(mMediaPlayer);
			mLyricView.setLyricFile(getLyricFile(music.getMediaPath().getPath()));

			if (!resetPosition && mCurrentTime != 0) {
				mMediaPlayer.seekTo(mCurrentTime);
			} else {
				mSeekBar.setProgress(0);
				mMusicCurrentTime.setText(CommonUtil.convertMsToHHMMSS(0));
			}
			mSeekBar.setMax(music.getDuration());

			if (startPlay) {
				mMediaPlayer.start();
				mVisualizerView.enableEqualizer();
			}
			updateToggleButton();
			updateShowView();
		} catch (IllegalArgumentException | SecurityException | IllegalStateException | IOException e) {
			Log.d(TAG, "PlayMusic Exception");
			e.printStackTrace();
			reInitView();
			mErrorMusicCounter.increase();
			if(mErrorMusicCounter.isLimitExceeded()) {
				return;
			}
			navigate(true, false, true);
			return;
		}
		mEnableUpdate = true;
		mErrorMusicCounter.clear();
	}

	private OnCompletionListener mOnCompletionListener = new OnCompletionListener() {

		@Override
		public void onCompletion(MediaPlayer mp) {
			Log.d(TAG, "onCompletion");
			updateToggleButton();
			navigate(true, false, true);
		}
	};

	private OnErrorListener mOnErrorListener = new OnErrorListener() {

		@Override
		public boolean onError(MediaPlayer mp, int what, int extra) {
			Log.d(TAG, "onError");
			navigate(true, false, true);
			return true;
		}
	};

	private void updateToggleButton() {
		if (mMediaPlayer == null) {
			mToggleButton.setState(ImageButtonWithState.STATE_PLAY);
			return;
		}
		if (mMediaPlayer.isPlaying()) {
			mToggleButton.setState(ImageButtonWithState.STATE_PAUSE);
		} else {
			mToggleButton.setState(ImageButtonWithState.STATE_PLAY);
		}
	}

	private void updateShowView() {
		LocalMusic music = getCurrentMusic();
		if (music == null) {
			mMusicTitle.setText(getString(R.string.text_music_title));
			mMusicCurrentTime.setText(getString(R.string.text_time_init));
			mMusicDuration.setText(getString(R.string.text_time_init));
			mMusicIndexView.setText(getString(R.string.text_music_playing_index_default));
			mMusicCountView.setText(getString(R.string.text_music_count_default));
			mSeekBar.setMax(0);
			mSeekBar.setProgress(0);
			return;
		}

		updateTitle(music);
		mMusicCurrentTime.setText(CommonUtil.convertMsToHHMMSS(mCurrentTime));
		mMusicDuration.setText(CommonUtil.convertMsToHHMMSS(music.getDuration()));
		mMusicIndexView.setText(getString(R.string.text_music_playing_index) + Integer.toString(mPlayRecord.getCurrentIndex() + 1));
		mMusicCountView.setText(getString(R.string.text_music_count) + Integer.toString(mMusicItemCache.size()));
		mSeekBar.setProgress(mCurrentTime);

		mMusicItemAdapter.notifyDataSetChanged();
	}

	private void startAutoUpdate() {
		mHandler.sendEmptyMessage(MESSAGE_AUTOUPDATE_TIME);
	}

	private void stopAutoUpdate() {
		mHandler.removeMessages(MESSAGE_AUTOUPDATE_TIME);
	}

	private Handler mHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MESSAGE_AUTOUPDATE_TIME:
				autoUpdate();
				int remaining = 500;
				if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
					remaining = 1000 - (mCurrentTime % 1000);
				}
				mHandler.sendEmptyMessageDelayed(MESSAGE_AUTOUPDATE_TIME, remaining);
				break;
			default:
				break;
			}
		}

	};

	private void autoUpdate() {
		if (isSeekBarTracking) {
			return;
		}

		if (mEnableUpdate && mMediaPlayer != null && mMediaPlayer.isPlaying()) {
			mCurrentTime = mMediaPlayer.getCurrentPosition() <= mMediaPlayer.getDuration() ? mMediaPlayer.getCurrentPosition() : mMediaPlayer.getDuration();
			mMusicCurrentTime.setText(CommonUtil.convertMsToHHMMSS(mCurrentTime));
			mSeekBar.setProgress(mCurrentTime);
		}
	}

	private OnSeekBarChangeListener mOnSeekBarChangeListener = new OnSeekBarChangeListener() {

		@Override
		public void onStopTrackingTouch(SeekBar seekBar) {
			isSeekBarTracking = false;
			if (mMediaPlayer != null) {
				mMediaPlayer.seekTo(mCurrentTime);
			}
		}

		@Override
		public void onStartTrackingTouch(SeekBar seekBar) {
			isSeekBarTracking = true;
		}

		@Override
		public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
			if (fromUser) {
				mCurrentTime = progress;
			}
			mMusicCurrentTime.setText(CommonUtil.convertMsToHHMMSS(progress));
		}
	};

	@Override
	public void onClick(View v) {
		mStopPlayInBackground = false;
		switch (v.getId()) {
		case R.id.music_toggle:
			togglePlay();
			break;
		case R.id.music_previous:
			navigate(false, true, true);
			break;
		case R.id.music_next:
			navigate(true, true, true);
			break;
		case R.id.music_loop:
			changeLoopMode();
			break;
		case R.id.back_button:
			goToHomePage();
			break;
		case R.id.button_adjust_volume:
			showVolumeDialog();
			break;
		default:
			break;
		}
	}

	private void togglePlay() {
		if (mMediaPlayer == null) {
			playMusic(false);
			return;
		}

		if (mMediaPlayer.isPlaying()) {
			mVisualizerView.enableDataProcess(false);
			mMediaPlayer.pause();
		} else {
			mVisualizerView.enableDataProcess(true);
			mMediaPlayer.start();
			mVisualizerView.enableEqualizer();
		}
		updateToggleButton();
	}

	private OnItemClickListener mOnItemClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			mStopPlayInBackground = false;
			playNewMusic(position);
		}

	};

	private void playNewMusic(int position) {
		mCurrentTime = 0;
		mPlayRecord.add(mPlayList.getItemByPosition(position));
		resetLyricView();
		playMusic(true);
	}

	private void navigate(boolean forward, boolean fromUser, boolean resetPostion) {
		resetLyricView();
		mCurrentTime = 0;
		int index = mPlayList.navigate(forward, fromUser);
		Log.d(TAG, "navigate index:"+index);
		if (index != -1) {
			mPlayRecord.add(mPlayList.getItem(index));
			updatePlayList(index);
			if(mStopPlayInBackground) {
				playMusic(resetPostion, false);
			} else {
				playMusic(resetPostion);
			}
		}
	}

	private void resetLyricView() {
		if (mLyricView != null) {
			mLyricView.reset();
		}
	}

	private void changeLoopMode() {
		mMusicLooper.change();
		mPlayList.reset();
		if (mMusicLooper.isRandomMode()) {
			mPlayList.shuffle();
		}
		updateRepeatButton();
	}

	private File getLyricFile(String musicFile) {
		String lyricName = musicFile.substring(0, musicFile.lastIndexOf('.')) + ".lrc";
		return new File(lyricName);
	}

	private void updateRepeatButton() {
		if (mMusicLooper.isLoopMode()) {
			mRepeatButton.setImageResource(R.drawable.owl_repeat_all_button);
			mMusicRepeatText.setText(mRepeatTexts[0]);
		} else if (mMusicLooper.isOrderMode()) {
			mRepeatButton.setImageResource(R.drawable.owl_repeat_disable_button);
			mMusicRepeatText.setText(mRepeatTexts[1]);
		} else if (mMusicLooper.isSingleMode()) {
			mRepeatButton.setImageResource(R.drawable.owl_repeat_one_button);
			mMusicRepeatText.setText(mRepeatTexts[2]);
		} else {
			mRepeatButton.setImageResource(R.drawable.owl_repeat_random_button);
			mMusicRepeatText.setText(mRepeatTexts[3]);
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		Log.d(TAG, "onSaveInstanceState");
		super.onSaveInstanceState(outState);
		savePlayParams();
	}

	@Override
	protected void updateContent() {
		super.updateContent();
		refreshPlayList();
	}

	class MusicPagerAdapter extends PagerAdapter {
		private List<View> mViewList;

		public MusicPagerAdapter(List<View> list) {
			mViewList = list;
		}

		@Override
		public int getCount() {
			return mViewList.size();
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view.equals(object);
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			container.addView(mViewList.get(position));
			return mViewList.get(position);
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView(mViewList.get(position));
		}

	}

	private void updatePlayList(int position) {
		if (mListView.getFirstVisiblePosition() <= mPlayRecord.getCurrentIndex() && mPlayRecord.getCurrentIndex() <= mListView.getLastVisiblePosition()) {
			return;
		}
		mListView.post(new Runnable() {

			@Override
			public void run() {
				mListView.setSelection(mPlayRecord.getCurrentIndex());
			}
		});
	}

	private void reInitView() {
		releaseMediaPlayer();
		resetCurrentTime();
		updateShowView();
		updatePlayList(mPlayRecord.getCurrentIndex());
		updateToggleButton();
	}
	
	private void refreshPlayList() {
		//TODO
		Log.d(TAG, "refreshPlayList");
		boolean prePlay = false;
		boolean startPlay = false;
		ListItem item = mPlayRecord.getCurrentItem();
		if(item!=null) {
			int newIndex = mPlayList.getIndexById(item);
			if(newIndex!=-1) {
				if(newIndex!=item.index){
					item.index = newIndex;
				}
			} else {
				Log.d(TAG, "can not find current music, reset");
				if(mMediaPlayer!=null && mMediaPlayer.isPlaying()) {
					startPlay = true;
				}
				releaseMediaPlayer();
				resetCurrentTime();
				if(mPlayList.getItemByPosition(0)!=null) {
					mPlayRecord.add(mPlayList.getItemByPosition(0));
				}
				prePlay = true;
			}
		}
		updateShowView();
		updatePlayList(mPlayRecord.getCurrentIndex());
		if(prePlay) {
			if(startPlay) {
				playMusic(true);
			} else {
				prePlayMusic(true);
			}
		}
		updateToggleButton();
	}

	private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			Log.d(TAG, "onReceive:"+action);
			if (action.equals(Intent.ACTION_SHUTDOWN)) {
				savePlayParams();
				releaseMediaPlayer();
				finish();
			}
		}
	};

	private void releaseMediaPlayer() {
		Log.d(TAG, "releaseMediaPlayer");
		if (mMediaPlayer != null) {
			if (mMediaPlayer.isPlaying()) {
				mMediaPlayer.stop();
			}
			mMediaPlayer.release();
			mMediaPlayer = null;
		}
		resetLyricView();
		mVisualizerView.releaseVisualizer();
	}

	private void resetCurrentTime() {
		mCurrentTime = 0;
	}

	private void updateTitle(LocalMusic music) {
		mMusicTitle.setText(String.format(getString(R.string.string_format_music_title), music.getTitle(), music.getArtist()));
	}

	private OnKeyListener mListViewOnKeyListener = new OnKeyListener() {

		@Override
		public boolean onKey(View v, int keyCode, KeyEvent event) {
			if (event.getAction() == KeyEvent.ACTION_DOWN) {
				switch (keyCode) {
				case KeyEvent.KEYCODE_DPAD_CENTER:
				case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:
				case KeyEvent.KEYCODE_ENTER:
					int position = mListView.getSelectedItemPosition();
					if (position != AdapterView.INVALID_POSITION && position != mPlayRecord.getCurrentIndex()) {
						playNewMusic(position);
						return true;
					}
					togglePlay();
					return true;
				default:
					break;
				}
			}
			return false;
		}
	};

	private void fastForwardAndBackward(boolean forward) {
		LocalMusic music = getCurrentMusic();
		if (music == null) {
			return;
		}

		int duration = music.getDuration();
		int delta = (int) (duration * Config.FAST_FORWARD_BACKWARD_INTERVAL);
		if (delta < 1000) {
			delta = 1000;
		}

		delta *= forward ? 1 : -1;
		int nextPosition = mMediaPlayer.getCurrentPosition() + delta;

		if (nextPosition > duration) {
			navigate(true, true, true);
			return;
		}

		if (nextPosition < 0) {
			nextPosition = 0;
		}

		if (mMediaPlayer != null) {
			mCurrentTime = nextPosition;
			mMediaPlayer.seekTo(nextPosition);
			mSeekBar.setProgress(nextPosition);
		}
	}

	private boolean mKeyLongPress = false;

	@Override
	public boolean onKeyLongPress(int keyCode, KeyEvent event) {
		mKeyLongPress = true;
		return super.onKeyLongPress(keyCode, event);
	}

	public void showVolumeDialog() {
		mAudioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_SAME, AudioManager.FLAG_SHOW_UI);
	}
	
	private void goToHomePage() {
		if(mMediaPlayer==null || !mMediaPlayer.isPlaying()) {
			finish();
			return;
		}
		Intent intent = new Intent();
		intent.setAction(Intent.ACTION_MAIN);
		intent.addCategory(Intent.CATEGORY_HOME);
		startActivity(intent);
	}
	
	private OnPlayListUpdateListener mOnPlayListUpdateListener = new OnPlayListUpdateListener() {
		
		@Override
		public void onPlayListUpdate() {
			Log.d(TAG, "onPlayListUpdate");
			refreshPlayList();
		}
	};
	
	private void toggleSpectrumAndLyric() {
		if(mViewPager!=null) {
			int count = mViewPager.getAdapter().getCount();
			int nextItem = (mViewPager.getCurrentItem() + 1) % count;
			mViewPager.setCurrentItem(nextItem);
		}
	}
}
