package com.actions.music.app;

public class Config {
	/** Preferences */
	public static final String PREFERENCE_SAVE_INDEX = "playlist_save_index";
	public static final String PREFERENCE_REPEAT_MODE = "music_repeat_mode";

	public static final int MAX_PLAY_RECORD = 20;
	public static final String PREFERENCE_SAVE_PLAYRECORD = "music_save_playlist";
	public static final String PREFERENCE_SAVE_BREAKPOINT = "music_save_breakpoint";
	
	public static final float FAST_FORWARD_BACKWARD_INTERVAL = (float) 0.05;
}
