package com.actions.miracast;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.net.NetworkInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.Window;
import android.view.WindowManager;

public class WFDDisplayActivity extends Activity {

	public static final String TAG = "WFDSinkDisplayActivity";
	private WFDSinkService mWFDSinkService = null;
	protected WFDSinkStateMachine mWFDSinkStateMachine = null;
	private SurfaceView mSV = null;
	private final IntentFilter intentFilter = new IntentFilter();
	private BroadcastReceiver receiver = null;
	private boolean destroyByUser = true;
	private boolean clientStarted = false;
	private boolean surfaceCreated = false;

    private final int EXIT_APP_DIALOG = 4;

	public void setDestroyByUser(boolean destroyByUser) {
		this.destroyByUser = destroyByUser;
	}

	private ServiceConnection mConnection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			Log.i(TAG, "WFDSinkService is connected.");
			mWFDSinkService = ((WFDSinkService.MyBinder) service).getService();
			if (mWFDSinkService.isWFDAVManagerConnected() == false) {
				Log.e(TAG, "Cannot find WFDAVManagerService.");
				if (isFinishing() == false) {
					finish();
				}
			}

			mWFDSinkStateMachine = mWFDSinkService.getWFDSinkStateMachine();

			if (clientStarted == false && surfaceCreated == true) {
				Log.i(TAG, "Start view after sink service connected. surface:"
						+ mSV.getHolder().getSurface().toString());
				mWFDSinkService.startClient(mSV.getHolder().getSurface());
				clientStarted = true;
			}

			// To keep this API build into APK for test.
			getCurrentState();
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			Log.e(TAG, "WFDSinkService is disconnected. " + isFinishing());
			if (isFinishing() == false) {
				finish();
			}
		}
	};

	// Callback for SurfaceView
	private Callback cb = new Callback() {
		@Override
		public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2,
				int arg3) {
			Log.i(TAG, "surfaceChanged" + arg1 + "  " + arg2 + "   " + arg3);
		}

		@Override
		public void surfaceCreated(SurfaceHolder holder) {
			Log.i(TAG, "surfaceCreated");
			surfaceCreated = true;
			if (mWFDSinkService != null && clientStarted == false) {
				Log.i(TAG, "Start view after Surface created. surface:"
						+ mSV.getHolder().getSurface().toString());
				mWFDSinkService.startClient(mSV.getHolder().getSurface());
				clientStarted = true;
			}
		}

		@Override
		public void surfaceDestroyed(SurfaceHolder holder) {
			Log.i(TAG, "surfaceDestroyed");
			surfaceCreated = false;
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "onCreate WFDDisplayActivity");
		super.onCreate(savedInstanceState);

		// add necessary intent values to be matched.
		intentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
		intentFilter
				.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);

		// full screen
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
				WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.sink_display);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		mSV = (SurfaceView) findViewById(R.id.my_surfaceview);
            


   
		this.bindService(new Intent(WFDDisplayActivity.this,
				WFDSinkService.class), mConnection, Context.BIND_AUTO_CREATE);

		receiver = new WFDDisplayActivityReceiver();
	}

	@Override
	public void onResume() {
		Log.d(TAG, "onResume WFDDisplayActivity");
		super.onResume();
		registerReceiver(receiver, intentFilter);
		mSV.getHolder().addCallback(cb);
	}

	@Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
        Log.e(TAG, "onConfigurationChanged : END");
    }

	@Override
	public void onPause() {
		Log.d(TAG, "onPause WFDDisplayActivity");
		super.onPause();
		unregisterReceiver(receiver);
		clientStarted = false;
		mSV.getHolder().removeCallback(cb);
		if (destroyByUser == true) {
			if (mWFDSinkStateMachine != null)
				mWFDSinkStateMachine.sendMessage(WFDSinkStateMachine.USER_DESTROY_SESSION);
		}
	//	setDestroyByUser(true);  fixme
	}

	@Override
	public void onStop() {
		Log.d(TAG, "onStop WFDDisplayActivity");
		super.onStop();
	}

	@Override
	public void onDestroy() {
		Log.d(TAG, "onDestroy WFDDisplayActivity");
		unbindService(mConnection);
		//mWFDSinkService.teardownClient();
		cb=null;
		mWFDSinkService = null;
		surfaceCreated = false;
		super.onDestroy();
	}

    @Override
	public void onBackPressed() {
		showDialog(EXIT_APP_DIALOG);
	}

    public Dialog onCreateDialog(int id) {
    	AlertDialog localAlertDialog = null;
        if (id == EXIT_APP_DIALOG) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.exit);
            builder.setMessage(R.string.exit_miracast);
            builder.setPositiveButton(getString(R.string.dlg_ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
            builder.setNegativeButton(getString(R.string.dlg_cancel), null);
            localAlertDialog = builder.create();
        }
        
        return localAlertDialog;
    }
    
	public String getCurrentState() {
		if (mWFDSinkStateMachine == null) {
			return null;
		}
		return mWFDSinkStateMachine.getState().getName();
	}

	/**
	 * A BroadcastReceiver that notifies of important WiFi display events.
	 */
	private class WFDDisplayActivityReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
	
			if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)) {
				// p2p is enabled/disabled.
				int state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1);
				if (state != WifiP2pManager.WIFI_P2P_STATE_ENABLED) {
					mWFDSinkStateMachine.sendMessage(WFDSinkStateMachine.USER_TURN_OFF_WIFI_DIRECT);
					if (isFinishing() == false) {
						Log.d(TAG, "Destory WFDACTIVITY XXXXXXXXXXXXXX to false");
						setDestroyByUser(false);
						finish();
					}
				}
				Log.d(TAG, "P2P state changed - " + state);
				
			} else if (WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action)) {
				// p2p connection is established/shutdown.
				NetworkInfo networkInfo = (NetworkInfo) intent.getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO);
				if (networkInfo.isConnected() == false) {
					if (isFinishing() == false) {
						Log.d(TAG, "Destory WFDACTIVITY XXXXXXXXXXXXXX 2  to false");
						setDestroyByUser(false);
						finish();
					}
				}
				Log.d(TAG, "P2P connection changed");
			}
		}
	}
}
