/*
 * This file is auto-generated.  DO NOT MODIFY.
 * Original file: /home/wangzh/mk2-wfd/vendor/rh/generic/wifidisplay/WiFiDisplayApplication/src/com/rh/wfd/IWFDSessionServerListener.aidl
 */
package com.actions.wfdinterface;
public interface IWFDSessionServerListener extends android.os.IInterface
{
/** Local-side IPC implementation stub class. */
public static abstract class Stub extends android.os.Binder implements com.actions.wfdinterface.IWFDSessionServerListener
{
private static final java.lang.String DESCRIPTOR = "com.actions.wfdinterface.IWFDSessionServerListener";
/** Construct the stub at attach it to the interface. */
public Stub()
{
this.attachInterface(this, DESCRIPTOR);
}
/**
 * Cast an IBinder object into an com.actions.wfdinterface.IWFDSessionServerListener interface,
 * generating a proxy if needed.
 */
public static com.actions.wfdinterface.IWFDSessionServerListener asInterface(android.os.IBinder obj)
{
if ((obj==null)) {
return null;
}
android.os.IInterface iin = (android.os.IInterface)obj.queryLocalInterface(DESCRIPTOR);
if (((iin!=null)&&(iin instanceof com.actions.wfdinterface.IWFDSessionServerListener))) {
return ((com.actions.wfdinterface.IWFDSessionServerListener)iin);
}
return new com.actions.wfdinterface.IWFDSessionServerListener.Stub.Proxy(obj);
}
public android.os.IBinder asBinder()
{
return this;
}
@Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
{
switch (code)
{
case INTERFACE_TRANSACTION:
{
reply.writeString(DESCRIPTOR);
return true;
}
case TRANSACTION_onWFDSessionServerCreated:
{
data.enforceInterface(DESCRIPTOR);
com.actions.wfdinterface.IWFDSessionServer _arg0;
_arg0 = com.actions.wfdinterface.IWFDSessionServer.Stub.asInterface(data.readStrongBinder());
this.onWFDSessionServerCreated(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_notify:
{
data.enforceInterface(DESCRIPTOR);
int _arg0;
_arg0 = data.readInt();
int _arg1;
_arg1 = data.readInt();
int _arg2;
_arg2 = data.readInt();
this.notify(_arg0, _arg1, _arg2);
reply.writeNoException();
return true;
}
case TRANSACTION_onWFDSessionServerDestroyed:
{
data.enforceInterface(DESCRIPTOR);
this.onWFDSessionServerDestroyed();
reply.writeNoException();
return true;
}
}
return super.onTransact(code, data, reply, flags);
}
private static class Proxy implements com.actions.wfdinterface.IWFDSessionServerListener
{
private android.os.IBinder mRemote;
Proxy(android.os.IBinder remote)
{
mRemote = remote;
}
public android.os.IBinder asBinder()
{
return mRemote;
}
public java.lang.String getInterfaceDescriptor()
{
return DESCRIPTOR;
}
// when IWFDSessionServer is created, this callback will be called

public void onWFDSessionServerCreated(com.actions.wfdinterface.IWFDSessionServer sessServ) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeStrongBinder((((sessServ!=null))?(sessServ.asBinder()):(null)));
mRemote.transact(Stub.TRANSACTION_onWFDSessionServerCreated, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
// notify

public void notify(int code, int arg0, int arg1) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeInt(code);
_data.writeInt(arg0);
_data.writeInt(arg1);
mRemote.transact(Stub.TRANSACTION_notify, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
// when IWFDSessionServer is destroyed, this callback will be called

public void onWFDSessionServerDestroyed() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_onWFDSessionServerDestroyed, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
}
static final int TRANSACTION_onWFDSessionServerCreated = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
static final int TRANSACTION_notify = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
static final int TRANSACTION_onWFDSessionServerDestroyed = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
}
// when IWFDSessionServer is created, this callback will be called

public void onWFDSessionServerCreated(com.actions.wfdinterface.IWFDSessionServer sessServ) throws android.os.RemoteException;
// notify

public void notify(int code, int arg0, int arg1) throws android.os.RemoteException;
// when IWFDSessionServer is destroyed, this callback will be called

public void onWFDSessionServerDestroyed() throws android.os.RemoteException;
}
