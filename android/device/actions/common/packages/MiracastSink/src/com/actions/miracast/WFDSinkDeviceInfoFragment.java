package com.actions.miracast;

import java.util.Random;

import android.util.Log;

/**
 * A ListFragment that displays available peers on discovery and requests the
 * parent activity to handle user interaction events
 */
public class WFDSinkDeviceInfoFragment{

	public static final String TAG = "WFDSinkDeviceInfoFragment";


public WFDSinkDeviceInfoFragment(){



}




	public void notifyPin(String pinCode) {
		Log.d(TAG, "recevied pin code : " + pinCode);
		if (pinCode != null) {
			
		} else {
			resetPin();
		}
	}

	public void resetPin() {
		Log.d(TAG, "reset pin code");
		String pin = getRandomPin();
	
	}

	public String getRandomPin() {
		Random rd = new Random();
		String pin = "";
		int rdGet;
		do {
			rdGet = Math.abs(rd.nextInt()) % 10 + 48;
			char num = (char) rdGet;
			String dd = Character.toString(num);
			pin += dd;
		} while (pin.length() < 8);

		int pinInt = Integer.valueOf(pin) % 10000000;
		int accum = 0;
		while (pinInt != 0) {
			accum += 3 * (pinInt % 10);
			pinInt /= 10;
			accum += pinInt % 10;
			pinInt /= 10;
		}
		int checkSum = (10 - accum % 10) % 10;
		int result = (Integer.valueOf(pin) % 10000000) * 10 + checkSum;
		String resultString = String.valueOf(result);
		while (resultString.length() < 8){
			resultString = "0" + resultString;
		}
		return resultString;
	}
}
