package com.actions.miracast;

import android.content.Intent;
import android.os.Message;
import android.util.Log;

import com.actions.miracast.util.IState;
import com.actions.miracast.util.State;
import com.actions.miracast.util.StateMachine;

public class WFDSinkStateMachine extends StateMachine {
	private static final String TAG = "WFDSinkStateMachine";

	// Message(what) to take an action
	//
	// User turns on WiFi Direct from setting menu
	public static final int USER_TURN_ON_WIFI_DIRECT = 1;
	// User turns off WiFi Direct from setting menu
	public static final int USER_TURN_OFF_WIFI_DIRECT = 2;
	// whether device information is registered or not
	public static final int REGISTER_DEVICE_INFO_RESULT = 7;
	// Register WFD device info timeout
	public static final int REGISTER_DEVICE_INFO_TIMEOUT = 8;
	// Destroy RTSP session
	public static final int USER_DESTROY_SESSION = 9;
	// P2P connection is established
	public static final int P2P_CONNECTION_ESTABLISHED = 10;
	// P2P connection is destroyed
	public static final int P2P_CONNECTION_DESTROYED = 11;
	// Start RTSP client
	public static final int START_RTSP_CLIENT = 12;
	// WFD session client is established
	public static final int SESSION_CLIENT_CREATED = 13;
	// WFD session client is destroyed
	public static final int SESSION_CLIENT_DESTROYED = 14;

	// Timeout value
	public static final long REGISTER_DEVICE_INFO_TIMEOUT_TIME = 2000;
	
	// Status
	private DisabledState mDisabledState;
	private InitiatingState mInitiatingState;
	private EnabledState mEnabledState;
	private DisconnectingState mDisconnectingState;
	private ConnectedState mConnectedState;
	private WaitingState mWaitingState;
	private DisplayingState mDisplayingState;

	// WFD Sink Service
	private WFDSinkService mWFDSinkService;

	// Constructor
	public WFDSinkStateMachine(WFDSinkService wfdSinkService) {
		super(TAG);

		mWFDSinkService = wfdSinkService;

		mDisabledState = new DisabledState();
		mInitiatingState = new InitiatingState();
		mEnabledState = new EnabledState();
		mDisconnectingState =new DisconnectingState();
		mConnectedState = new ConnectedState();
		mWaitingState = new WaitingState();
		mDisplayingState = new DisplayingState();

		addState(mDisabledState);
		addState(mInitiatingState);
		addState(mEnabledState);
		addState(mDisconnectingState, mEnabledState);
		addState(mConnectedState, mEnabledState);
		addState(mWaitingState, mConnectedState);
		addState(mDisplayingState, mWaitingState);

		setInitialState(mDisabledState);
	}

	public IState getState(){
		return this.getCurrentState();
	}
	
	private class DisabledState extends State {
		@Override
		public void enter() {
			Log.d(TAG, "Enter DisabledState: " + getCurrentMessage().what);
			mWFDSinkService.showNotification(R.string.DisabledState);
		}

		@Override
		public boolean processMessage(Message message) {
			Log.d(TAG, "DisabledState process message: " + message.what);

			boolean retValue = HANDLED;

			switch (message.what) {
			case USER_TURN_ON_WIFI_DIRECT:
				transitionTo(mInitiatingState);
				break;
			default:
				return NOT_HANDLED;
			}
			return retValue;
		}
	}

	private class InitiatingState extends State {
		@Override
		public void enter() {
			Log.d(TAG, "Enter InitiatingState: " + getCurrentMessage().what);
			mWFDSinkService.showNotification(R.string.InitiatingState);
			mWFDSinkService.registerWFDDeviceInfo();
		}

		@Override
		public boolean processMessage(Message message) {
			Log.d(TAG, "InitiatingState process message: " + message.what);

			boolean retValue = HANDLED;

			switch (message.what) {
			case USER_TURN_OFF_WIFI_DIRECT:
				transitionTo(mDisabledState);
				break;
			case REGISTER_DEVICE_INFO_RESULT:
				removeMessages(REGISTER_DEVICE_INFO_TIMEOUT);
				if ((Boolean) message.obj) {
					transitionTo(mEnabledState);
				} else {
					transitionTo(mDisabledState);
				}
				break;
			case REGISTER_DEVICE_INFO_TIMEOUT:
				transitionTo(mDisabledState);
				break;
			default:
				return NOT_HANDLED;
			}
			return retValue;
		}
	}

	private class EnabledState extends State {
		@Override
		public void enter() {
			Log.d(TAG, "Enter EnabledState: " + getCurrentMessage().what);
			mWFDSinkService.showNotification(R.string.EnabledState);
			mWFDSinkService.resetWFDDeviceInfo(true);
		}

		@Override
		public boolean processMessage(Message message) {
			Log.d(TAG, "EnabledState process message: " + message.what);

			boolean retValue = HANDLED;

			switch (message.what) {
			case USER_TURN_OFF_WIFI_DIRECT:
				transitionTo(mDisabledState);
				break;
			case P2P_CONNECTION_ESTABLISHED:
				transitionTo(mConnectedState);
				break;
			default:
				return NOT_HANDLED;
			}
			return retValue;
		}
	}

	private class DisconnectingState extends State {
		@Override
		public void enter() {
			Log.d(TAG, "Enter DisconnectingState: " + getCurrentMessage().what);
			mWFDSinkService.showNotification(R.string.DisconnectingState);
			mWFDSinkService.destroyP2pConnection();
		}

		@Override
		public boolean processMessage(Message message) {
			Log.d(TAG, "DisconnectingState process message: " + message.what);

			boolean retValue = HANDLED;

			switch (message.what) {
			case P2P_CONNECTION_DESTROYED:
				transitionTo(mEnabledState);
				break;
			default:
				return NOT_HANDLED;
			}
			return retValue;
		}
	}
	
	private class ConnectedState extends State {
		@Override
		public void enter() {
			Log.d(TAG, "Enter ConnectedState: " + getCurrentMessage().what);
			mWFDSinkService.showNotification(R.string.ConnectedState);
			mWFDSinkService.resetWFDDeviceInfo(false);
		}

		@Override
		public boolean processMessage(Message message) {
			Log.d(TAG, "ConnectedState process message: " + message.what);

			boolean retValue = HANDLED;

			switch (message.what) {
			case START_RTSP_CLIENT:
				transitionTo(mWaitingState);
				break;
			case USER_DESTROY_SESSION:
				Log.d(TAG, "receive USER_DESTROY_SESSION");
				transitionTo(mDisconnectingState);
				break;
			case P2P_CONNECTION_DESTROYED:
				transitionTo(mEnabledState);
				break;
			case P2P_CONNECTION_ESTABLISHED:
				Log.d(TAG, "ignore P2P_CONNECTION_ESTABLISHED in ConnectedState");
				break;
			default:
				return NOT_HANDLED;
			}
			return retValue;
		}
	}

	private class WaitingState extends State {
		@Override
		public void enter() {
			Log.d(TAG, "Enter WaitingState: " + getCurrentMessage().what);
			mWFDSinkService.showNotification(R.string.WaitingState);
            Intent intent = new Intent(mWFDSinkService, WFDDisplayActivity.class);  
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			mWFDSinkService.startActivity(intent);
		}

		@Override
		public boolean processMessage(Message message) {
			Log.d(TAG, "WaitingState process message: " + message.what);

			boolean retValue = HANDLED;

			switch (message.what) {
			case SESSION_CLIENT_CREATED:
				transitionTo(mDisplayingState);
				break;
			case START_RTSP_CLIENT:
				Log.d(TAG, "ignore START_RTSP_CLIENT in WaitingState");
				break;
			case P2P_CONNECTION_ESTABLISHED:
				Log.d(TAG, "ignore P2P_CONNECTION_ESTABLISHED in WatingState");
				break;
			default:
				return NOT_HANDLED;
			}
			return retValue;
		}

		@Override
		public void exit() {
			Log.d(TAG, "EXIT WaitingState");
			mWFDSinkService.resetAVManager();
		}
	}

	private class DisplayingState extends State {
		@Override
		public void enter() {
			Log.d(TAG, "Enter DisplayingState: " + getCurrentMessage().what);
			mWFDSinkService.showNotification(R.string.DisplayingState);
		}

		@Override
		public boolean processMessage(Message message) {
			Log.d(TAG, "DisplayingState process message: " + message.what);

			boolean retValue = HANDLED;

			switch (message.what) {
			case SESSION_CLIENT_DESTROYED:
				transitionTo(mDisconnectingState);
				break;
			case START_RTSP_CLIENT:
				Log.d(TAG, "ignore START_RTSP_CLIENT in DisplayingState");
				break;
			case P2P_CONNECTION_ESTABLISHED:
				Log.d(TAG, "ignore P2P_CONNECTION_ESTABLISHED in DisplayingState");
				break;
			default:
				return NOT_HANDLED;
			}
			return retValue;
		}

		@Override
		public void exit() {
			Log.d(TAG, "EXIT DisplayingState");
			mWFDSinkService.teardownClient();
		}
	}
}
