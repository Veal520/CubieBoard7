package com.actions.settings.widget;

import static android.provider.Settings.System.SCREEN_OFF_TIMEOUT;

import com.actions.settings.R;

import android.content.ContentResolver;
import android.content.Context;
import android.preference.Preference;
import android.provider.Settings;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class SleepPreference extends Preference implements android.widget.CompoundButton.OnCheckedChangeListener,OnClickListener {
	private static final String TAG = "SleepPreference";
	/*** use this max screen time out time  */
	private static final int MAX_TIME_OUT = 2147483647;
	private static final int FALLBACK_SCREEN_TIMEOUT_VALUE = 30000;
	private static final int SCREEN_TIMEOUT_1 = 15*1000;
	private static final int SCREEN_TIMEOUT_2 = 30 * 1000;
	private static final int SCREEN_TIMEOUT_3 = 60*1000;
	private static final int SCREEN_TIMEOUT_4 = 120*1000;
	private static final int SCREEN_TIMEOUT_5 = 300*1000;
	
	private RadioGroup radioGroup1;
	private RadioButton mRadioButton1;
	private RadioButton mRadioButton2;
	private RadioButton mRadioButton3;
	private RadioButton mRadioButton4;
	private RadioButton mRadioButton5;
	private RadioButton mRadioButton6;
	private Context mContext;

	public SleepPreference(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		mContext = context;
	}

	public SleepPreference(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
		setLayoutResource(R.layout.sleep_prefence);
		// TODO Auto-generated constructor stub
	}

	public SleepPreference(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		mContext = context;
		// TODO Auto-generated constructor stub
		setLayoutResource(R.layout.sleep_prefence);
	}

	@Override
	protected View onCreateView(ViewGroup parent) {
		// TODO Auto-generated method stub
		return super.onCreateView(parent);

	}

	@Override
	protected void onBindView(View view) {
		// TODO Auto-generated method stub
		super.onBindView(view);
		mRadioButton1 = (RadioButton) view.findViewById(R.id.radio1);
		mRadioButton2 = (RadioButton) view.findViewById(R.id.radio2);
		mRadioButton3 = (RadioButton) view.findViewById(R.id.radio3);
		mRadioButton4 = (RadioButton) view.findViewById(R.id.radio4);
		mRadioButton5 = (RadioButton) view.findViewById(R.id.radio5);
		mRadioButton6 = (RadioButton) view.findViewById(R.id.radio6);
		mRadioButton1.setOnCheckedChangeListener(this);
		mRadioButton2.setOnCheckedChangeListener(this);
		mRadioButton3.setOnCheckedChangeListener(this);
		mRadioButton4.setOnCheckedChangeListener(this);
		mRadioButton5.setOnCheckedChangeListener(this);
		mRadioButton6.setOnCheckedChangeListener(this);
		
		updataState();
	}

	private void updataState(){
		final ContentResolver resolver = mContext.getContentResolver();
		 final long currentTimeout = Settings.System.getLong(resolver, SCREEN_OFF_TIMEOUT,
	                FALLBACK_SCREEN_TIMEOUT_VALUE);
		 Log.i(TAG, "currentTimeout:"+currentTimeout);
		 
		 if(currentTimeout == SCREEN_TIMEOUT_1){
			 mRadioButton1.setChecked(true);
		 }else if(currentTimeout == SCREEN_TIMEOUT_2){
			 mRadioButton2.setChecked(true);
		 } else if(currentTimeout == SCREEN_TIMEOUT_3){
			 mRadioButton3.setChecked(true);
		 }else if(currentTimeout == SCREEN_TIMEOUT_4){
			 mRadioButton4.setChecked(true);
		 }else if(currentTimeout == SCREEN_TIMEOUT_5){
			 mRadioButton5.setChecked(true);
		 }else if(currentTimeout == MAX_TIME_OUT){
			 mRadioButton6.setChecked(true);
		 }
	}

	@Override
	public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
		// TODO Auto-generated method stub
		
		int value = 15 * 1000;
		switch (arg0.getId()) {
		case R.id.radio1:
			if (arg1)
			value = SCREEN_TIMEOUT_1;
			break;
		case R.id.radio2:
			if (arg1)
			value = SCREEN_TIMEOUT_2;
			break;
		case R.id.radio3:
			if (arg1)
			value = SCREEN_TIMEOUT_3;
			break;
		case R.id.radio4:
			if (arg1)
			value = SCREEN_TIMEOUT_4;
			break;
		case R.id.radio5:
			if (arg1)
			value = SCREEN_TIMEOUT_5;
			break;
		case R.id.radio6:
			if (arg1)
			value = MAX_TIME_OUT;
			break;	
		default:
			break;
		}
//		Log.i(TAG, "vcalue:"+value);
//		Log.i(TAG, "arg1:"+arg1);
		if(arg1){
			freshCheck(arg0);		
			Settings.System.putInt(mContext.getContentResolver(),
					SCREEN_OFF_TIMEOUT, value);
		}
		
	}
	
	void freshCheck(CompoundButton radio){
		RadioButton[] RadioButtons = {mRadioButton1,mRadioButton2,mRadioButton3,mRadioButton4,mRadioButton5,mRadioButton6};
		for (int i =0;i < RadioButtons.length; i ++){
			if (RadioButtons[i] == null){
				return;
			}
			if (radio.getId() == RadioButtons[i].getId() ){
				//RadioButtons[i].setChecked(true);
				Log.i(TAG, "i:"+i);
				continue;
			}else {
				RadioButtons[i].setChecked(false);
			}
		}
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		
	}
}
