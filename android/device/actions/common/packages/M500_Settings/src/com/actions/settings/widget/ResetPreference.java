package com.actions.settings.widget;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.preference.Preference;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;

import com.actions.settings.R;
import com.android.internal.os.storage.ExternalStorageFormatter;

public class ResetPreference extends Preference implements OnClickListener {
	private static final String TAG = "ResetPreference";

	private final Context mContext;
	private CheckBox clearFlashCheck;

	private ImageButton resetButton;

	public ResetPreference(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		mContext = context;
	}

	public ResetPreference(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
		setLayoutResource(R.layout.factory_reset_prefence);
		// TODO Auto-generated constructor stub
	}

	public ResetPreference(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		// TODO Auto-generated constructor stub
		mContext = context;
		setLayoutResource(R.layout.factory_reset_prefence);
	}

	@Override
	protected View onCreateView(ViewGroup parent) {
		// TODO Auto-generated method stub
		return super.onCreateView(parent);

	}

	@Override
	protected void onBindView(View view) {
		// TODO Auto-generated method stub
		super.onBindView(view);
		clearFlashCheck = (CheckBox) view.findViewById(R.id.clearFlashCheck);
		resetButton = (ImageButton) view.findViewById(R.id.resetButton);
		resetButton.setOnClickListener(this);
	}

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		if (view.getId() == R.id.resetButton) {
			showWarnningDialog();
		}
	}
	private void showWarnningDialog(){
		AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
		builder.setTitle(R.string.preference_reset_format_dialog_title);
		builder.setMessage(R.string.preference_reset_format_dialog_message);
		builder.setPositiveButton(R.string.preference_reset_format_dialog_PositiveButton, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				// TODO Auto-generated method stub
				doMasterClear();
			}
		});
		
		builder.setNegativeButton(R.string.preference_reset_format_dialog_NegativeButton, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				// TODO Auto-generated method stub
				
			}
		});
		builder.show();  
	}

	private void doMasterClear() {
		if (clearFlashCheck.isChecked()) {
			Intent intent = new Intent(
					ExternalStorageFormatter.FORMAT_AND_FACTORY_RESET);
			intent.putExtra(Intent.EXTRA_REASON, "MasterClearConfirm");
			intent.setComponent(ExternalStorageFormatter.COMPONENT_NAME);
			mContext.startService(intent);
		} else {
			Intent intent = new Intent(Intent.ACTION_MASTER_CLEAR);
			intent.addFlags(Intent.FLAG_RECEIVER_FOREGROUND);
			intent.putExtra(Intent.EXTRA_REASON, "MasterClearConfirm");
			mContext.sendBroadcast(intent);
			// Intent handling is asynchronous -- assume it will happen soon.
		}
	}
}
