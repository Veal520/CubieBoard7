/*
 * @author Actions Semi.
 */

package com.actions.explorer;

import java.io.File;

import android.app.IntentService;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore.Audio;
import android.provider.MediaStore.Files;
import android.provider.MediaStore.Files.FileColumns;
import android.provider.MediaStore.Images;
import android.provider.MediaStore.Images.ImageColumns;
import android.provider.MediaStore.MediaColumns;
import android.provider.MediaStore.Video;
import android.provider.MediaStore.Audio.AudioColumns;
import android.provider.MediaStore.Video.VideoColumns;
import android.util.Log;

public class RefreshMediaService extends IntentService {
	long time;
	long startNotify;
	static final String EXTERNAL_VOLUME = "external";
	public static final String ACTION_MEDIA_SCANNER_SCAN_DIRECTORY = "android.intent.action.MEDIA_SCANNER_SCAN_DIR";
	public static final int MODE_DELETE = 0;
	public static final int MODE_ADD = 1;
	private static final String TAG = "RefreshMedia";

	public RefreshMediaService() {
		super("FeRefreshMediaService");
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();

		Log.d("mydebug", "oncreate:" + "start service");

	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub

		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		Log.v(TAG, "--onHandleIntent--");
		String file = intent.getStringExtra("mediaPath");
		Log.d("notify delete mydebug", file);
		int mode = intent.getIntExtra("mode",MODE_DELETE);

		startNotify = System.currentTimeMillis();
		switch (mode) {
		case MODE_DELETE:
			notifyMediaDelete(file);
			break;

		case MODE_ADD:
			Log.i(TAG, "MODE_ADD");
			notifyMediaAdd(file);
			break;
		}

	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub

		super.onDestroy();
	}

	public void notifyMediaDelete(String file) {

		Uri[] mediatypes = new Uri[] { Audio.Media.getContentUri(EXTERNAL_VOLUME), Video.Media.getContentUri(EXTERNAL_VOLUME),
				Images.Media.getContentUri(EXTERNAL_VOLUME), Files.getContentUri(EXTERNAL_VOLUME), };
		ContentResolver cr = getApplicationContext().getContentResolver();		
		int a = 0;		
		for (int i = 0; i < mediatypes.length; i++) {
			switch (i) {
			case 0:
				a = cr.delete(mediatypes[i],AudioColumns.DATA + "=?", new String[]{file}); 

				break;
			case 1:
				a = cr.delete(mediatypes[i],VideoColumns.DATA + "=?", new String[]{file}); 

				break;
			case 2:
				a = cr.delete(mediatypes[i],ImageColumns.DATA + "=?", new String[]{file}); 

				break;
			case 3:
				a = cr.delete(mediatypes[i],FileColumns.DATA + "=?", new String[]{file}); 

				break;
			}

		}

		Log.d("mydebug", "notifyMediaDelete:" + file + "crud result" + a);

	}

	public void notifyMediaAdd(String file) {
		File mfile = new File(file);
		if (mfile.exists()) {
			/*
			 * notify the media to scan
			 */
			Uri mUri = Uri.fromFile(mfile);
			Intent mIntent = new Intent();
			mIntent.setData(mUri);
			if (mfile.isDirectory()) {
				// Log.d("mydebug","notifyMediaAdd dir:"+file);
				mIntent.setAction(ACTION_MEDIA_SCANNER_SCAN_DIRECTORY);
			} else {
				// Log.d("mydebug","notifyMediaAdd file:"+file);
				mIntent.setAction(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
			}
			getApplicationContext().sendBroadcast(mIntent);
		}
		

		
	
	}

}
