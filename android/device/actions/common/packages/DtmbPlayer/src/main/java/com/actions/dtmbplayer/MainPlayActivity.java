package com.actions.dtmbplayer;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.FragmentTransaction;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.media.AudioManager;
import android.media.AudioManager.OnAudioFocusChangeListener;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SoundEffectConstants;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.widget.ImageView;

import com.actions.dtmbplayer.saveinfo.DtmbStreamRecorder;
import com.actions.dtmbplayer.saveinfo.EventCounter;
import com.actions.dtmbplayer.saveinfo.EventCounter.OnEventCountEnoughListener;
import com.actions.dtmbplayer.widget.DtmbPlayer;
import com.actions.dtmbplayer.widget.DtmbPlayer.OnMediaReleaseListener;
import com.actions.dtmbplayer.widget.ProgramListFragment;
import com.actions.dtmbplayer.widget.TestFragment;

public class MainPlayActivity extends Activity implements OnClickListener {
	private static final String TAG = MainPlayActivity.class.getSimpleName();

	private static final int MESSAGE_HIDE_BACKGROUND = 0;
	private static final int MESSAGE_SHOW_BACKGROUND = 1;

	private DtmbPlayer mDtmbPlayer;
	private SurfaceView mMovieView;
	private ProgramListFragment mListFragment;
	private ImageView mImageViewBackground;
	private AudioManager mAudioManager;
	private EventCounter mEventCounter;
	private DtmbStreamRecorder mDtmbStreamRecorder;
	private AlertDialog mDebugAlertDialog;
	private boolean mBound = false;
	private Messenger mLogcatService;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "onCreate");
		super.onCreate(savedInstanceState);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.activity_main_play_layout);
		addProgramList();
		if (Config.DEBUG) {
			addTestFragment();
		}
		initView();
		initControl();
		initEventCount();
	}

	private void initView() {
		mMovieView = (SurfaceView) findViewById(R.id.surfaceview_movie);
		mImageViewBackground = (ImageView) findViewById(R.id.imageview_play_default_bg);
		mGestureDetector = new GestureDetector(this, new SimpleOnGestureListener() {
			@Override
			public boolean onSingleTapConfirmed(MotionEvent e) {
				Log.d(TAG, "onSingleTapConfirmed");
				toggleMenu(false);

				return true;
			}

			@Override
			public boolean onSingleTapUp(MotionEvent e) {
				Log.d(TAG, "onSingleTapUp");
				mEventCounter.count();
				return super.onSingleTapUp(e);
			}

		});
		mMovieView.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				mGestureDetector.onTouchEvent(event);
				return true;
			}
		});
	}

	private void initControl() {
		mDtmbPlayer = DtmbPlayer.getInstance(this);
		mDtmbPlayer.setSurfaceView(mMovieView);
		mDtmbPlayer.setOnCompletionListener(new OnCompletionListener() {

			@Override
			public void onCompletion(MediaPlayer mp) {
				showDefaultBackground();
			}
		});
		mDtmbPlayer.setOnErrorListener(new OnErrorListener() {

			@Override
			public boolean onError(MediaPlayer mp, int what, int extra) {
				showDefaultBackground();
				return true;
			}
		});
		mDtmbPlayer.setOnPreparedListener(new OnPreparedListener() {

			@Override
			public void onPrepared(MediaPlayer mp) {
				Log.d(TAG, "hideDefaultBackground");
				hideDefaultBackground(0);
			}
		});
		mDtmbPlayer.setOnMediaReleaseListener(new OnMediaReleaseListener() {

			@Override
			public void onMediaRelease() {
				Log.d(TAG, "showDefaultBackground 1s delay");
				showDefaultBackground(1000);
			}
		});
	}

	private void addProgramList() {
		mListFragment = new ProgramListFragment();
		FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
		fragmentTransaction.replace(R.id.program_list_fragment, mListFragment);
		fragmentTransaction.commit();
	}

	private void addTestFragment() {
		View testFragment = findViewById(R.id.test_fragment);
		testFragment.setVisibility(View.VISIBLE);
		FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
		fragmentTransaction.replace(R.id.test_fragment, new TestFragment());
		fragmentTransaction.commit();
	}

	@Override
	protected void onResume() {
		Log.d(TAG, "onResume");
		stopBackgroundMusic();
		super.onResume();
	}

	@Override
	protected void onPause() {
		Log.d(TAG, "onPause");
		super.onPause();
		mAudioManager.abandonAudioFocus(mAudioFocusChangeListener);
	}

	@Override
	protected void onDestroy() {
		Log.d(TAG, "onDestroy");
		super.onDestroy();
		dismissDebugDialog();
		mHandler.removeMessages(MESSAGE_HIDE_BACKGROUND);
		releaseDebugSave();
		mDtmbPlayer.release();

	}

	private void dismissDebugDialog() {
		if (mDebugAlertDialog != null) {
			mDebugAlertDialog.dismiss();
			mDebugAlertDialog = null;
		}
	}

	@Override
	public void onClick(View v) {

	}

	private GestureDetector mGestureDetector;

	private void toggleMenu(boolean keepShow) {
		if (mListFragment != null) {
			mListFragment.toggleMenu(keepShow);
		}
	}

	private void showDefaultBackground() {
		if (!mImageViewBackground.isShown()) {
			mImageViewBackground.setVisibility(View.VISIBLE);
		}
	}

	public void showDefaultBackground(int delay) {
		mHandler.removeMessages(MESSAGE_HIDE_BACKGROUND);
		mHandler.removeMessages(MESSAGE_SHOW_BACKGROUND);
		mHandler.sendEmptyMessageDelayed(MESSAGE_SHOW_BACKGROUND, delay);
	}

	private void hideDefaultBackground(int delay) {
		mHandler.removeMessages(MESSAGE_HIDE_BACKGROUND);
		mHandler.removeMessages(MESSAGE_SHOW_BACKGROUND);
		mHandler.sendEmptyMessageDelayed(MESSAGE_HIDE_BACKGROUND, delay);
	}

	private void hideDefaultBackground() {
		if (mImageViewBackground.isShown()) {
			mImageViewBackground.setVisibility(View.GONE);
		}
	}

	private Handler mHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MESSAGE_HIDE_BACKGROUND:
				hideDefaultBackground();
				break;
			case MESSAGE_SHOW_BACKGROUND:
				showDefaultBackground();
				break;
			default:
				break;
			}
		}

	};

	private void stopBackgroundMusic() {
		if (mAudioManager == null) {
			mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		}
		int result = mAudioManager.requestAudioFocus(mAudioFocusChangeListener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
		if (result == AudioManager.AUDIOFOCUS_REQUEST_FAILED) {
			Log.i(TAG, "request Audio focus failed");
		}
	}

	private OnAudioFocusChangeListener mAudioFocusChangeListener = new OnAudioFocusChangeListener() {

		@Override
		public void onAudioFocusChange(int focusChange) {
			switch (focusChange) {
			case AudioManager.AUDIOFOCUS_LOSS:
			case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
			case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
				break;
			default:
				break;
			}
		}
	};

	private void initEventCount() {
		mEventCounter = new EventCounter();
		mEventCounter.setLimit(7);
		mEventCounter.setOnEventCountEnoughListener(mOnEventCountEnoughListener);
	}

	private OnEventCountEnoughListener mOnEventCountEnoughListener = new OnEventCountEnoughListener() {

		@Override
		public void onEventCountEnough() {
			showSaveDataCheckDialog();
		}
	};

	private void showSaveDataCheckDialog() {
		Builder builder = new AlertDialog.Builder(MainPlayActivity.this);
		builder.setTitle(R.string.message_save_debug_info);
		builder.setCancelable(false);
		builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				initDebugSave();
			}
		});
		builder.setNegativeButton(android.R.string.cancel, null);
		mDebugAlertDialog = builder.create();
		mDebugAlertDialog.show();
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		Log.d(TAG, "onKeyUp");
		switch (keyCode) {
		case KeyEvent.KEYCODE_DPAD_CENTER:
			mEventCounter.count();
			break;
		case KeyEvent.KEYCODE_MENU:
			toggleMenu(true);
			playClickSoundEffect();
			return true;
		}
		return super.onKeyUp(keyCode, event);
	}

	private void initDebugSave() {
		startLogcatService();
		if (mDtmbStreamRecorder == null) {
			mDtmbStreamRecorder = new DtmbStreamRecorder(MainPlayActivity.this);
			mDtmbStreamRecorder.StartRecord();
		}
	}

	private void startLogcatService() {
		Intent intent = new Intent(Config.ACTION_SERVICE_CATLOG);
		intent.setPackage(Config.SERVICE_PACKAGE_NAME);
		bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);
	}

	private void releaseDebugSave() {
		if (mDtmbStreamRecorder != null) {
			mDtmbStreamRecorder.stopRecord();
			mDtmbStreamRecorder = null;
		}
		releaseLogcatService();
	}

	private ServiceConnection mServiceConnection = new ServiceConnection() {

		@Override
		public void onServiceDisconnected(ComponentName name) {
			Log.d(TAG, "onServiceDisconnected");
			mLogcatService = null;
			mBound = false;
		}

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			Log.d(TAG, "onServiceConnected");
			mLogcatService = new Messenger(service);
			mBound = true;
			sendStartLogcatCommand();
		}
	};

	private void sendStartLogcatCommand() {
		if (mLogcatService != null) {
			Bundle data = new Bundle();
			data.putString(Config.KEY_PACKAGE_NAME, MainPlayActivity.this.getPackageName());
			Message message = Message.obtain(null, Config.MESSAGE_COMMAND_START_LOGCAT, data);
			try {
				mLogcatService.send(message);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
	}

	private void sendStopLogcatCommand() {
		if (mLogcatService != null) {
			Message message = Message.obtain(null, Config.MESSAGE_COMMAND_STOP_LOGCAT);
			try {
				mLogcatService.send(message);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
	}

	private void releaseLogcatService() {
		if (mBound) {
			sendStopLogcatCommand();
			unbindService(mServiceConnection);
			mBound = false;
		}
	}
	
	public void playClickSoundEffect() {
		if(getWindow().getDecorView()!=null) {
			getWindow().getDecorView().playSoundEffect(SoundEffectConstants.CLICK);
		}
	}
}
