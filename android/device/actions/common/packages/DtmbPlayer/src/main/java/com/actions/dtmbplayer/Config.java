package com.actions.dtmbplayer;

public class Config {
	public static boolean DEBUG = false;

	public static final String KEY_PACKAGE_NAME = "packageName";
	public static final String ACTION_SERVICE_CATLOG = "com.actions.catlog.service.LogcatService";
	public static final String SERVICE_PACKAGE_NAME = "com.actions.catlog";
	public static final int MESSAGE_COMMAND_START_LOGCAT = 0;
	public static final int MESSAGE_COMMAND_STOP_LOGCAT = 1;
}
