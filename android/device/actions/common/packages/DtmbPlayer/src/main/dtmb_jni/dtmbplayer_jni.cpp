#define LOG_TAG "DtmbPlayer-JNI"
#include <media/mediaplayer.h>
#include <media/IMediaHTTPService.h>
#include <media/MediaPlayerInterface.h>
#include "utils/Log.h"
#include <stdlib.h>
#include "jni.h"
#include <stdio.h>
#include <limits.h>
#include <unistd.h>
#include <fcntl.h>
#include <utils/threads.h>
#include "JNIHelp.h"
#include "android_runtime/AndroidRuntime.h"
#include "utils/Errors.h"  // for status_t
#include "utils/String8.h"

// dtmb header
extern "C"{
	#include "libdrm_alogrithm.h"
}

#include "dtmb.h"
#include "DtmbJNI.h"
#include "parser.h"


using namespace android;

const int MEDIA_DATA_ERROR = -1;
const int MEDIA_CHANNEL_AVAIABLE = 0;
const int MEDIA_SCAN_FINISH = 1;


typedef struct{
	jclass clazz;
	jfieldID frequencyFieldId;
	jfieldID programIdFieldId;
	jfieldID programNameFieldId;
	jmethodID initMethodId;
} ChannelFieldAndMethod;

static ChannelFieldAndMethod *mChannelFieldAndMethod;
static DtmbController *mDtmbStreamSource = NULL;

static bool DEBUG = true;

static void initClassFieldAndMethod(JNIEnv *env, jobject dtmbPlayer) {
	mChannelFieldAndMethod = new ChannelFieldAndMethod();

	jclass channelclazz = env->FindClass("com/actions/dtmbplayer/data/Channel");
	mChannelFieldAndMethod->clazz = (jclass)env->NewGlobalRef(channelclazz);
	mChannelFieldAndMethod->frequencyFieldId = env->GetFieldID(mChannelFieldAndMethod->clazz, "frequency", "I");
	mChannelFieldAndMethod->programIdFieldId = env->GetFieldID(mChannelFieldAndMethod->clazz, "programId", "I");
	mChannelFieldAndMethod->programNameFieldId = env->GetFieldID(mChannelFieldAndMethod->clazz, "programName", "Ljava/lang/String;");	
	mChannelFieldAndMethod->initMethodId = env->GetMethodID(mChannelFieldAndMethod->clazz, "<init>", "()V");
}

static MediaPlayer* getMediaPlayer(JNIEnv *env,jobject mediaplayer){
	jclass clazz;
	jfieldID context;
    clazz = env->FindClass("android/media/MediaPlayer");
    if (clazz == NULL) {
        return NULL;
    }

    context = env->GetFieldID(clazz, "mNativeContext", "J");
    if (context == NULL) {
        return NULL;
    }
	
	MediaPlayer* const p = (MediaPlayer*)env->GetLongField(mediaplayer, context);
	return p;
}


static jobject createNewChannel(JNIEnv *env) {
	if(DEBUG) ALOGD("createNewChannel");
	return env->NewObject(mChannelFieldAndMethod->clazz, mChannelFieldAndMethod->initMethodId);
}

static void fillChannelContent(JNIEnv *env, jobject channel, int freq, int programId, unsigned char* programName) {
	if(DEBUG) ALOGD("fillChannelContent");
	env->SetIntField(channel, mChannelFieldAndMethod->frequencyFieldId, freq);
	env->SetIntField(channel, mChannelFieldAndMethod->programIdFieldId, programId);

	jstring mProgramNameValue = env->NewStringUTF(reinterpret_cast<const char*>(programName));
	env->SetObjectField(channel, mChannelFieldAndMethod->programNameFieldId, mProgramNameValue);
}

static jobjectArray createNewArray(JNIEnv *env, int length) {
	if(DEBUG) ALOGD("createNewArray");
	ALOGD("size:%d", length);
	jobjectArray array = env->NewObjectArray(length, mChannelFieldAndMethod->clazz, NULL);
	return array;
}
static jobjectArray com_actions_dtmbplayer_widget_dtmbplayer_getFreqInfo(JNIEnv *env, jobject thiz, jint freq) {
	channels_info cf = getFreqInfo(freq);
	if(DEBUG) ALOGD("freq=%d", freq);

	if(cf.tot_num <=0) {
		return NULL;		
	}
	if(DEBUG) ALOGD("program count:%d", cf.tot_num);
	jobjectArray data = createNewArray(env, cf.tot_num);
	for(int i=0; i<cf.tot_num; i++) {
		channel* cn = cf.chanstr+i;
		jobject javaChannelObject = createNewChannel(env);
		fillChannelContent(env, javaChannelObject, freq, cn->program_num, cn->chanstr);
		env->SetObjectArrayElement(data, i, javaChannelObject);
	}
	free(cf.chanstr);
	return data;
}

static void com_actions_dtmbplayer_widget_dtmbplayer_setup(JNIEnv *env, jobject thiz) {
	if(DEBUG) ALOGD("setup");
	if(acts_software_piracy_protection()!=0) {
		return;
	}
	initClassFieldAndMethod(env, thiz);
	mDtmbStreamSource = openDTMB();
}

static void com_actions_dtmbplayer_widget_dtmbplayer_setDataSource(JNIEnv *env, jobject thiz, jobject mediaplayer) {
	if(DEBUG) ALOGD("setDataSource");
	MediaPlayer* mp = getMediaPlayer(env, mediaplayer);
	mp->setDataSource(getSource());
}

static jint com_actions_dtmbplayer_widget_dtmbplayer_setChannel(JNIEnv *env, jobject thiz, jint frequency, jint programId, jboolean forceLock) {
	if(DEBUG) ALOGD("setChannel:   frequency=%d    programID=%d", frequency, programId);
	int result = setChannel(frequency, programId, forceLock);
	return result;
}

static void com_actions_dtmbplayer_widget_dtmbplayer_release(JNIEnv *env, jobject thiz) {
	if(DEBUG) ALOGD("release");
	mDtmbStreamSource = NULL;
	env->DeleteGlobalRef(mChannelFieldAndMethod->clazz);
	delete mChannelFieldAndMethod;
	int result = releaseDTMB();
}

static jint com_actions_dtmbplayer_widget_dtmbplayer_getBER(JNIEnv *env, jobject thiz) {
	if(DEBUG) ALOGD("getBER:%d", getBER());
	return getBER();
}

static jboolean com_actions_dtmbplayer_widget_dtmbplayer_getLockedStatus(JNIEnv *env, jobject thiz) {
	if(DEBUG) ALOGD("getLockedStatus");
	return getLockedStatus();
}

static void com_actions_dtmbplayer_widget_dtmbplayer_clearSource(JNIEnv * env,jobject thiz) {
	if(DEBUG) ALOGD("clearSource");
	clearSource();
}

static void com_actions_dtmbplayer_widget_dtmbplayer_setDebugStreamFilePath(JNIEnv * env, jobject thiz, jstring path) {
	const char *pathStr = env->GetStringUTFChars(path, NULL);
	if(DEBUG) {
		ALOGD("setDebugStreamFilePath:%s", pathStr);
	}
	setDebugFileName(pathStr);
	env->ReleaseStringUTFChars(path, pathStr);
}

static void com_actions_dtmbplayer_widget_dtmbplayer_startDebugRecord(JNIEnv * env,jobject thiz) {
	if(DEBUG) ALOGD("startDebugRecord");
	startLog();
}

static void com_actions_dtmbplayer_widget_dtmbplayer_stopDebugRecord(JNIEnv * env,jobject thiz) {
	if(DEBUG) ALOGD("stopDebugRecord");
	stopLog();
}


static JNINativeMethod gMethods[] = {
		{"native_setup", "()V", (void *)com_actions_dtmbplayer_widget_dtmbplayer_setup},
		{"native_setDataSource", "(Landroid/media/MediaPlayer;)V", (void *)com_actions_dtmbplayer_widget_dtmbplayer_setDataSource},
		{"native_setChannel", "(IIZ)I", (void *)com_actions_dtmbplayer_widget_dtmbplayer_setChannel},
		{"native_release", "()V", (void *)com_actions_dtmbplayer_widget_dtmbplayer_release},
		{"native_getBER", "()I", (void *)com_actions_dtmbplayer_widget_dtmbplayer_getBER},
		{"native_getLockedStatus", "()Z", (void *)com_actions_dtmbplayer_widget_dtmbplayer_getLockedStatus},
		{"native_getFreqInfo", "(I)[Ljava/lang/Object;", (void *)com_actions_dtmbplayer_widget_dtmbplayer_getFreqInfo},
		{"native_clearSource", "()V", (void *)com_actions_dtmbplayer_widget_dtmbplayer_clearSource},
		{"native_setDebugStreamFilePath", "(Ljava/lang/String;)V", (void *)com_actions_dtmbplayer_widget_dtmbplayer_setDebugStreamFilePath},
		{"native_startDebugRecord", "()V", (void *)com_actions_dtmbplayer_widget_dtmbplayer_startDebugRecord},
		{"native_stopDebugRecord", "()V", (void *)com_actions_dtmbplayer_widget_dtmbplayer_stopDebugRecord}
};

static const char* const kClassPathName = "com/actions/dtmbplayer/widget/DtmbPlayer";

static int register_com_actions_dmbplayer_widget_DtmbPlayer(JNIEnv *env) {
	return AndroidRuntime::registerNativeMethods(env, kClassPathName, gMethods, NELEM(gMethods));
}

jint JNI_OnLoad(JavaVM *vm, void *reserved) {
	JNIEnv *env = NULL;
	jint result = -1;

	if (vm->GetEnv((void**) &env, JNI_VERSION_1_4) != JNI_OK) {
		ALOGE("ERROR: GetEnv failed\n");
		goto fail;
	}

	assert(env != NULL);

	if (register_com_actions_dmbplayer_widget_DtmbPlayer(env) < 0) {
		ALOGE("ERROR: DtmbPlayer native registration failed\n");
		goto fail;
	}
	
	//	success -- return valid version number
	result = JNI_VERSION_1_4;
	
fail:
	return result;
}
