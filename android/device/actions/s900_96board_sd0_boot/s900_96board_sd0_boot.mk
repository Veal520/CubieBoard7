# Copyright (C) 2011 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#
# This file is the build configuration for a full Android
# build for toro hardware. This cleanly combines a set of
# device-specific aspects (drivers) with a device-agnostic
# product configuration (apps). Except for a few implementation
# details, it only fundamentally contains two inherit-product
# lines, full and toro, hence its name.
#

# Set zygote for 32bit
PRODUCT_DEFAULT_PROPERTY_OVERRIDES += ro.zygote=zygote32

# Inherit from those products. Most specific first.
$(call inherit-product, device/actions/common/gsbase_product.mk)
$(call inherit-product, device/actions/s900_96board_sd0_boot/device.mk)
$(call inherit-product, device/actions/common/prebuilt/gpu/s900/gpu.mk)
$(call inherit-product, device/actions/common/prebuilt/codec/s900/actcodec.mk)

PRODUCT_NAME := s900_96board_sd0_boot
PRODUCT_DEVICE := s900_96board_sd0_boot
PRODUCT_BRAND := Actions
PRODUCT_MODEL := S900 
