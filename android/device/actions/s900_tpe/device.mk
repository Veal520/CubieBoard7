#
# Copyright (C) 2011 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#---------------PART1----------------: board config 
R_ANDROID_DPI=hdpi
R_WIFI_TYPE=ap6212
R_BT_TYPE=ap6212
R_GMS_TYPE=base
R_FIRMWARE_ROOTED=true
ACTIONS_RELEASE_BUILD_FOR_CUSTOM=false
VERSION_DATE=$(shell date "+%y%m%d")


#---------------PART2----------------: system config 
PRODUCT_TAGS += dalvik.gc.type-precise
PRODUCT_CHARACTERISTICS := tablet
DEVICE_PACKAGE_OVERLAYS := $(LOCAL_PATH)/overlay
PRODUCT_RUNTIMES := runtime_libart_default
PRODUCT_AAPT_CONFIG += normal large $(R_ANDROID_DPI)
PRODUCT_AAPT_PREF_CONFIG := $(R_ANDROID_DPI)


#---------------PART3----------------: product copy file 
PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/kernel:kernel \
	$(LOCAL_PATH)/init.rc:root/init.rc \
	$(LOCAL_PATH)/ueventd.rc:root/ueventd.rc \
	$(LOCAL_PATH)/init.s900.usb.rc:root/init.s900.usb.rc \
	$(LOCAL_PATH)/init.eth0.rc:root/init.eth0.rc \
	$(LOCAL_PATH)/ueventd.s900.rc:root/ueventd.s900.rc \
	$(LOCAL_PATH)/fstab.s900:root/fstab.s900 \
	$(LOCAL_PATH)/init.s900.rc:root/init.s900.rc \
    $(LOCAL_PATH)/cp_vendor_app.sh:root/cp_vendor_app.sh \
    $(LOCAL_PATH)/init.modules.rc:root/init.modules.rc \
    $(LOCAL_PATH)/usbmond.sh:root/usbmond.sh \
	$(LOCAL_PATH)/gslX680.idc:system/usr/idc/gslX680.idc \
	$(LOCAL_PATH)/media_profiles.xml:system/etc/media_profiles.xml \
	$(LOCAL_PATH)/readme:system/vendor/app/readme \
	$(LOCAL_PATH)/apns-conf.xml:system/etc/apns-conf.xml \
	$(LOCAL_PATH)/ft5x06-touch.idc:system/usr/idc/ft5x06-touch.idc \
	$(LOCAL_PATH)/GT813.idc:system/usr/idc/GT813.idc \
	$(LOCAL_PATH)/gl5203-adckey.kl:system/usr/keylayout/gl5203-adckey.kl \
	$(LOCAL_PATH)/excluded-input-devices.xml:system/etc/excluded-input-devices.xml \
	$(LOCAL_PATH)/packages-compat-default.xml:system/etc/packages-compat-default.xml \
	$(LOCAL_PATH)/excluded_recovery:system/etc/excluded_recovery \
	$(LOCAL_PATH)/NOTICE.html:system/etc/NOTICE.html \
	$(LOCAL_PATH)/android.hardware.bluetooth.xml:system/etc/permissions/android.hardware.bluetooth.xml
	
PRODUCT_COPY_FILES += $(call add-to-product-copy-files-if-exists,\
	$(LOCAL_PATH)/bootanimation.zip:system/media/bootanimation.zip)
PRODUCT_COPY_FILES += $(call add-to-product-copy-files-if-exists,\
	$(LOCAL_PATH)/bootanimation_poweroff.zip:system/media/bootanimation_poweroff.zip)
PRODUCT_COPY_FILES += $(call add-to-product-copy-files-if-exists,\
	$(LOCAL_PATH)/poweron.mp3:system/etc/poweron.mp3)
PRODUCT_COPY_FILES += $(call add-to-product-copy-files-if-exists,\
	$(LOCAL_PATH)/poweron.mp3:system/etc/poweroff.mp3)
	
# hardware features
PRODUCT_COPY_FILES += \
	frameworks/native/data/etc/tablet_core_hardware.xml:system/etc/permissions/tablet_core_hardware.xml \
	frameworks/native/data/etc/android.hardware.touchscreen.multitouch.jazzhand.xml:system/etc/permissions/android.hardware.touchscreen.multitouch.jazzhand.xml \
	frameworks/native/data/etc/android.hardware.usb.accessory.xml:system/etc/permissions/android.hardware.usb.accessory.xml \
	frameworks/native/data/etc/android.hardware.usb.host.xml:system/etc/permissions/android.hardware.usb.host.xml \
	frameworks/native/data/etc/android.hardware.audio.low_latency.xml:system/etc/permissions/android.hardware.audio.low_latency.xml \
	frameworks/native/data/etc/android.hardware.ethernet.xml:system/etc/permissions/android.hardware.ethernet.xml \
	frameworks/native/data/etc/android.hardware.wifi.xml:system/etc/permissions/android.hardware.wifi.xml \
	frameworks/native/data/etc/android.hardware.wifi.direct.xml:system/etc/permissions/android.hardware.wifi.direct.xml \
	frameworks/native/data/etc/android.hardware.camera.front.xml:system/etc/permissions/android.hardware.camera.front.xml \
	frameworks/native/data/etc/android.hardware.camera.autofocus.xml:system/etc/permissions/android.hardware.camera.autofocus.xml \
	frameworks/native/data/etc/android.hardware.bluetooth.xml:system/etc/permissions/android.hardware.bluetooth.xml \
	frameworks/native/data/etc/android.hardware.bluetooth_le.xml:system/etc/permissions/android.hardware.bluetooth_le.xml \
	frameworks/native/data/etc/android.hardware.bluetooth.xml:system/etc/permissions/extras/android.hardware.bluetooth.xml \
	frameworks/native/data/etc/android.hardware.bluetooth_le.xml:system/etc/permissions/extras/android.hardware.bluetooth_le.xml \
	frameworks/native/data/etc/android.hardware.location.gps.xml:system/etc/permissions/extras/android.hardware.location.gps.xml \
	frameworks/native/data/etc/android.hardware.sensor.compass.xml:/system/etc/permissions/extras/android.hardware.sensor.compass.xml \
	frameworks/native/data/etc/android.hardware.sensor.gyroscope.xml:system/etc/permissions/extras/android.hardware.sensor.gyroscope.xml \
	frameworks/native/data/etc/android.hardware.sensor.light.xml:system/etc/permissions/extras/android.hardware.sensor.light.xml

# software features
PRODUCT_COPY_FILES += \
	frameworks/native/data/etc/android.software.sip.voip.xml:system/etc/permissions/android.software.sip.voip.xml \
	frameworks/native/data/etc/android.software.connectionservice.xml:system/etc/permissions/android.software.connectionservice.xml \
	frameworks/native/data/etc/android.software.webview.xml:system/etc/permissions/android.software.webview.xml \
    frameworks/av/media/libeffects/data/audio_effects.conf:system/etc/audio_effects.conf \
    frameworks/av/services/audiopolicy/audio_policy.conf:system/etc/audio_policy.conf \
    frameworks/av/media/libstagefright/data/media_codecs_google_audio.xml:system/etc/media_codecs_google_audio.xml \
    frameworks/av/media/libstagefright/data/media_codecs_google_telephony.xml:system/etc/media_codecs_google_telephony.xml \
    frameworks/av/media/libstagefright/data/media_codecs_google_video.xml:system/etc/media_codecs_google_video.xml \
	build/target/product/security/platform.pk8:/system/etc/security/platform.pk8 \
	build/target/product/security/platform.x509.pem:/system/etc/security/platform.x509.pem \
	external/ppp/dns_conf/ip-up:/system/etc/ppp/ip-up \
	external/ppp/dns_conf/ip-down:/system/etc/ppp/ip-down \
	packages/wallpapers/LivePicker/android.software.live_wallpaper.xml:system/etc/permissions/android.software.live_wallpaper.xml \
    system/core/rootdir/init.zygote64_32.rc:root/init.zygote64_32.rc


#---------------PART4----------------: product packages
PRODUCT_PACKAGES += \
	ActionsCamera \
	Actionslogcat \
	actionslogcat \
	cjpeg \
	djpeg \
	getevent_iio \
	i2cdetect \
	injectevent \
	inv_self_test-shared \
	perf \
	pppoe \
	acc_policy \
	gpu_config \
	sepolicy.recovery \
	interrupter \
	libjni_action_tinyplanet \
	invoke_mock_media_player \
    make_ext4fs \
	com.android.future.usb.accessory \
	recovery \
	updater \
    Camera2 \
    e2fsck \
    mkfs.f2fs \
    fsck.f2fs \
    Bluetooth \
	actionsframework \
	libgralloc \
	libtinyxml \
	locales_list.txt \
	libbt-vendor \
	bt_vendor.conf \
	bcm43438a0.hcd \
	audio.a2dp.default \
    PinyinIME \
    actions_prebuilt_apks \
    actions_prebuilt_codec \
    superuser_prebuilt \
    mbrc_checksum \
    check_app \
    upgrade_app \
    pad_vm \
	fw_bcmdhd.bin \
	fw_bcm43438a0.bin \
	fw_bcmdhd_p2p.bin \
	fw_bcm43438a0_p2p.bin \
	fw_bcmdhd_apsta.bin \
	fw_bcm43438a0_apsta.bin \
	nvram.txt \
	dhcpcd.conf \
	wpa_supplicant \
	wpa_supplicant.conf \
	wpa_supplicant_overlay.conf \
	p2p_supplicant_overlay.conf \
	init.wifi.rc \
    Launcher2 \
    OWLPlayer \
    FusedLocation \
    InputDevices \
    Keyguard \
    LatinIME \
    Phone \
    PrintSpooler \
    Provision \
    CtsAutoSetting \
    Settings \
    displayd \
    SystemUI \
    TeleService \
    WAPPushManager \
    audio \
    audio_policy.default \
    audio.primary.default \
    audio.r_submix.default \
    com.android.future.usb.accessory \
    hostapd \
    librs_jni \
    libvideoeditor_core \
    libvideoeditor_jni \
    libvideoeditor_osal \
    libvideoeditorplayer \
    libvideoeditor_videofilters \
    lint \
    local_time.default \
    network \
    pand \
    power.default \
    sdptool \
    vibrator.default \
    wpa_supplicant.conf \
    libGLES_android \
    actions \
    pfmnceserver \
    libperformance \
    libactions_runtime \
	charger \
	charger_res_images \
	SpeechRecorder \
	libsrec_jni \
    boot_driver \
    system_xbin \
    system_etc \
	dosfslabel \
	WifiTrafficMonitor \
	update   \
	ActExplore \
	ActSensorCalib \
	libdisplay.S900 \
    lights.S900 \
    camera.S900 \
	sensors.S900 \
	sensors.S900.GYRO \
    libvrapi \
    libdrm \
    libadf \
    libadfhwc \
    libadf_dynamic \
    libadfhwc_dynamic \
	libstagefrighthw \
	libOMX.Action.Video.Decoder \
	libOMX.Action.Audio.Decoder \
	libOMX.Action.Video.Decoder.Deinterlace \
	libOMX.Action.Video.Encoder \
	libOMX.Action.Video.Camera \
	libOMX.Action.Image.Decoder \
	libOMX_Core \
	libACT_V4L2HAL \
	libACT_ISP_PARM \
	libACT_IMX_PARM \
	libACT_EncAPI \
	libACT_VceResize \
	ping \
	netperf \
	netserver \
	tcpdump \
	wpa_cli \
	strace \
	rild \
	angk_001 \
	angk_002 \
	libactions-ril \
	usb_modeswitch \
	usb_modeswitch.d \
	libusb \
	libusb-compat \
	treadahead


#---------------PART5----------------: property config 
# this part move to system.prop
ADDITIONAL_BUILD_PROPERTIES += dalvik.vm.dex2oat-flags=""
PRODUCT_PROPERTY_OVERRIDES := \
	ro.build.display.id=TAG_AD900B_5110_$(VERSION_DATE) \
	ro.carrier=unknown \
	ro.com.android.dateformat=MM-dd-yyyy \
	ro.config.ringtone=Ring_Synth_04.ogg \
	ro.config.notification_sound=pixiedust.ogg \
	persist.sys.media.avsync=true \
	ro.setupwizard.mode=DISABLE \
	dalvik.vm.dexopt-flags=m=y \
	dalvik.vm.heapstartsize=8m \
	dalvik.vm.heapsize=384m \
	dalvik.vm.heaptargetutilization=0.75 \
	dalvik.vm.heapminfree=512k \
	dalvik.vm.heapmaxfree=8m \
	dalvik.vm.heapgrowthlimit=128m \
	dalvik.vm.checkjni=false \
	ro.product.locale.language=zh \
	ro.product.locale.region=CN
# ui rotation config
PRODUCT_DEFAULT_PROPERTY_OVERRIDES += \
    ro.ctp.rec_rotation=90 \
    ro.sf.hwrotation=270 \
    ro.sf.hdmi_rotation=180 \
    ro.sf.default_rotation=1
#---------------PART6----------------: call makefiles
$(call inherit-product, frameworks/native/build/tablet-7in-hdpi-1024-dalvik-heap.mk)
$(call inherit-product, build/target/product/core_base.mk)
$(call inherit-product-if-exists, frameworks/webview/chromium/chromium.mk)
$(call inherit-product-if-exists, frameworks/base/data/keyboards/keyboards.mk)
$(call inherit-product-if-exists, frameworks/base/data/fonts/fonts.mk)
$(call inherit-product-if-exists, frameworks/base/data/sounds/AudioPackage5.mk)
$(call inherit-product-if-exists, device/actions/common/prebuilt/widevine/widevine.mk)
ifeq ($(R_GMS_TYPE),core)
$(call inherit-product-if-exists, device/actions/common/prebuilt/apk/google/products/gms_core.mk)
else ifeq ($(R_GMS_TYPE),base)
$(call inherit-product-if-exists, device/actions/common/prebuilt/apk/google/products/gms_base.mk)
else ifeq ($(R_GMS_TYPE),advance)
$(call inherit-product-if-exists, device/actions/common/prebuilt/apk/google/products/gms_advance.mk)
else ifeq ($(R_GMS_TYPE),full)
$(call inherit-product-if-exists, device/actions/common/prebuilt/apk/google/products/gms.mk)
else
$(call inherit-product-if-exists, device/actions/common/prebuilt/apk/google/products/gms_base.mk)
endif

