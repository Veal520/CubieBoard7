#include <stdlib.h>
#include <stdint.h>
#include <sys/types.h>
#include <cutils/log.h>
#include <math.h>

#include "LandScapeCoordsMapper.h"

namespace ActsVR {
LandScapeCoordsMapper::LandScapeCoordsMapper() :
    mRot(0) {
}

LandScapeCoordsMapper::~LandScapeCoordsMapper() {
}

void LandScapeCoordsMapper::setOrientation(uint32_t orientation) {
    if ((orientation & Transform::ROT_270) == Transform::ROT_270) {
        mRot = __ROT_270;
    } else if ((orientation & Transform::ROT_180) == Transform::ROT_180) {
        mRot = __ROT_180;
    } else if ((orientation & Transform::ROT_90) == Transform::ROT_90) {
        mRot = __ROT_90;
    } else {
        mRot = __ROT_0;
    }
}

void LandScapeCoordsMapper::mappingScreen2Virtual(float screenWidth,
        float screenHeight, vec2* screenPositions, float* virtualPositions,
        uint32_t rotation) {
    if (!(!(rotation & Transform::ROT_90))) {
        for (size_t i = 0; i < 4; i++) {
            virtualPositions[i * 2] = screenPositions[i].x;
            virtualPositions[i * 2 + 1] = screenHeight - screenPositions[i].y;
        }
    } else {
        for (size_t i = 0; i < 4; i++) {
            virtualPositions[i * 2] = screenPositions[i].y;
            virtualPositions[i * 2 + 1] = screenPositions[i].x;
        }
    }
}

void LandScapeCoordsMapper::mappingVirtual2Screen(float screenWidth,
        float screenHeight, float* virtualPositions, vec2* screenPositions,
        uint32_t rotation) {
    for (size_t i = 0; i < 4; i++) {
        size_t i2 = i * 2;
        screenPositions[i].x = virtualPositions[i2];
        screenPositions[i].y = screenHeight - virtualPositions[i2 + 1];
    }
}

};//namespace ActsVR
