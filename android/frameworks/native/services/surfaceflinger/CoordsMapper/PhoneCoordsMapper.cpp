#include <stdlib.h>
#include <stdint.h>
#include <sys/types.h>
#include <cutils/log.h>
#include <math.h>

#include "PhoneCoordsMapper.h"
namespace ActsVR {
PhoneCoordMapper::PhoneCoordMapper() :
    mRot(0) {
}

PhoneCoordMapper::~PhoneCoordMapper() {
}

void PhoneCoordMapper::setOrientation(uint32_t orientation) {
    if ((orientation & Transform::ROT_270) == Transform::ROT_270) {
        mRot = __ROT_270;
    } else if ((orientation & Transform::ROT_180) == Transform::ROT_180) {
        mRot = __ROT_180;
    } else if ((orientation & Transform::ROT_90) == Transform::ROT_90) {
        mRot = __ROT_90;
    } else {
        mRot = __ROT_0;
    }
}

void PhoneCoordMapper::mappingScreen2Virtual(float screenWidth,
        float screenHeight, vec2* screenPositions, float* virtualPositions,
        uint32_t rotation) {
    switch (mRot) {
    case __ROT_0:
    case __ROT_180:
        for (size_t i = 0; i < 4; i++) {
            virtualPositions[i * 2] = screenPositions[i].x;
            virtualPositions[i * 2 + 1] = screenWidth - screenPositions[i].y;
        }
        break;
    case __ROT_90:
        for (size_t i = 0; i < 4; i++) {
            virtualPositions[i * 2] = screenWidth - screenPositions[i].y;
            virtualPositions[i * 2 + 1] = screenHeight - screenPositions[i].x;
        }
        break;
    case __ROT_270:
        for (size_t i = 0; i < 4; i++) {
            virtualPositions[i * 2] = screenPositions[i].y;
            virtualPositions[i * 2 + 1] = screenPositions[i].x;
        }
        break;
    }
}

void PhoneCoordMapper::mappingVirtual2Screen(float screenWidth,
        float screenHeight, float* virtualPositions, vec2* screenPositions,
        uint32_t rotation) {
        //screen coords map to virtual coords
    switch (mRot) {
    case __ROT_0:
    case __ROT_90:
        for (size_t i = 0; i < 4; i++) {
            size_t i2 = i * 2;
            screenPositions[i].x = screenHeight - virtualPositions[i2 + 1];
            screenPositions[i].y = screenWidth - virtualPositions[i2];
        }
        break;
    case __ROT_180:
    case __ROT_270:
        for (size_t i = 0; i < 4; i++) {
            size_t i2 = i * 2;
            screenPositions[i].x = virtualPositions[i2 + 1];
            screenPositions[i].y = virtualPositions[i2];
        }
        break;
    }
}

};//namespace ActsVR
