#ifndef __COORDS_MAPPER_H__
#define __COORDS_MAPPER_H__

#include <stdint.h>
#include <sys/types.h>
#include <string.h>
#include <ui/vec2.h>

#include <utils/Thread.h>

#include "Transform.h"
#include "CoordsMapper.h"

using namespace android;
namespace ActsVR {

#define __ROT_0 0
#define __ROT_90 1
#define __ROT_180 2
#define __ROT_270 3

class CoordsMapper {
protected:
    CoordsMapper();
    virtual ~CoordsMapper() = 0;
public:

    static CoordsMapper* create();

    void coordsMapping(float scaleX, float scaleY, float offsetY,
            float offsetX_left, float offsetX_right, float * coordsIn,
            size_t coordsCount, float * coordsOutLeft, float * coordsOutRight);
    void texCoordsMapping(float screenWidth, float screenHight, float resWidth, float resHeight,
            float texWidth, float texHeight, float* posLeft, float* posRight,
            vec2* texCoords, vec2* texCoordsLeft, vec2* texCoordsRight,
            bool* leftVisible, bool* rightVisible, bool vrenabled);

    void virtual setOrientation(uint32_t orientation)=0;
    void virtual mappingScreen2Virtual(float screenWidth, float screenHeight,
            vec2* screenPositions, float* virtualPositions,
            uint32_t rotation)=0;
    void virtual mappingVirtual2Screen(float screenWidth, float screenHeight,
            float* virtualPositions, vec2* screenPositions,
            uint32_t rotation)=0;
};

};// namespace ActsVR
#endif //__COORDS_MAPPER_H__
