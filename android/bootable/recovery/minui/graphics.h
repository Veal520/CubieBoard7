/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _GRAPHICS_H_
#define _GRAPHICS_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include "minui.h"
#include <drm/drm.h>

struct owl_adf_overlay_engine_capability_ext {
	__u32 support_transform;
	__u32 support_blend;
} __attribute__((packed, aligned(8)));

struct owl_adf_interface_data_ext {
	__u32 realWidth;
	__u32 realHeight;
} __attribute__((packed, aligned(8)));

struct owl_adf_buffer_config_ext {
	/* KERNEL unique identifier for any gralloc allocated buffer. */
	__u64 stamp;

	/* Which interface id that the buffer belongs to, added for convenience. */
	__u32 aintf_id;

	/* Window buffer property flag */
	__u32 flag;

	/* Crop applied to surface (BEFORE transformation) */
	struct drm_clip_rect	crop;

	/* Region of screen to display surface in (AFTER scaling) */
	struct drm_clip_rect	display;

	/* Surface rotation / flip / mirror */
	__u32			transform;

	/* Alpha blending mode e.g. none / premult / coverage */
	__u32			blend_type;

	/* Plane alpha */
	__u8			plane_alpha;
	__u8			reserved[3];
} __attribute__((packed, aligned(8)));

struct owl_adf_post_ext {
	__u32 post_id;
	__u32 flag;
	struct owl_adf_buffer_config_ext bufs_ext[];
} __attribute__((packed, aligned(8)));

struct owl_adf_validate_config_ext {
	__u32 n_interfaces;
	__u32 __user *interfaces;

	__u32 n_bufs;
	struct adf_buffer_config __user *bufs;

	struct owl_adf_post_ext __user *post_ext;
} __attribute__((packed, aligned(8)));


typedef struct minui_backend {
    // Initializes the backend and returns a gr_surface to draw into.
    gr_surface (*init)(struct minui_backend*);

    // Causes the current drawing surface (returned by the most recent
    // call to flip() or init()) to be displayed, and returns a new
    // drawing surface.
    gr_surface (*flip)(struct minui_backend*);

    // Blank (or unblank) the screen.
    void (*blank)(struct minui_backend*, bool);

    // Device cleanup when drawing is done.
    void (*exit)(struct minui_backend*);
} minui_backend;

void roate_framebuf(char *dst, char *src);

minui_backend* open_fbdev();
minui_backend* open_adf();

#ifdef __cplusplus
}
#endif

#endif
